select cod_thema, cod_rubrica, rubrica as name,  
		convert(integer, select count(*) from books where rubrices.cod_thema like books.cod_thema or rubrices.cod_thema like books.cod_rubrica) as countThema,    
		convert(integer, select count(*) from books where rubrices.cod_rubrica like books.cod_rubrica) as countRubrica, 
		convert(boolean, rubrices.cod_thema = rubrices.cod_rubrica) as isThema, 
		convert(boolean, (select count(*) from rubrices as thema_rubrices where rubrices.cod_thema = thema_rubrices.cod_thema) > 1) as hasRubrices 
from rubrices order by cod_thema, cod_rubrica

select cast(count(*) as UNSIGNED INTEGER) as count
from rubrices order by cod_thema, cod_rubrica

select * from books where cod_thema is null limit 25

insert into book_rubrica 
(book_id, thema_id, cod_thema, rubrica_id, cod_rubrica)
values(1, 82, 'tangeo', 86, 'tmat');

insert into book_rubrica 
(book_id, thema_id, cod_thema, rubrica_id, cod_rubrica)
values(2, 82, 'tangeo', 86, 'tmat');

insert into book_rubrica 
(book_id, thema_id, cod_thema, rubrica_id, cod_rubrica)
values(3, 82, 'tangeo', 86, 'tmat');

insert into book_rubrica 
(book_id, thema_id, cod_thema, rubrica_id, cod_rubrica)
values(42, 82, 'tangeo', 86, 'tmat');

insert into book_rubrica 
(book_id, thema_id, cod_thema, rubrica_id, cod_rubrica)
values(42, 15, 'tgum', 22, 'tangl');

insert into book_rubrica 
(book_id, thema_id, cod_thema, rubrica_id, cod_rubrica)
values(4, 82, 'tangeo', 86, 'tmat');

insert into book_rubrica 
(book_id, thema_id, cod_thema, rubrica_id, cod_rubrica)
values(4, 2, 'tbiograf', 2, 'tbiograf');

insert into book_rubrica 
(book_id, thema_id, cod_thema, rubrica_id, cod_rubrica)
values(4, 11, 'tvoina', 12, 'tsrazh');

select * from book_rubrica;
