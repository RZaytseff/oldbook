CREATE DATABASE oldbooks;

CREATE USER 'PiterOldBook'@'localhost' IDENTIFIED BY 'ADanilov'; 
GRANT ALL PRIVILEGES ON oldbooks.* To 'PiterOldBook'@'localhost' IDENTIFIED BY 'ADanilov'; 
FLUSH PRIVILEGES;

use oldbooks;

create table themes (
	id INT unique NOT NULL AUTO_INCREMENT,
    COD_THEMA CHAR(20),
    THEMA CHAR(255) unique,
	PRIMARY KEY (id),
);

create table rubrices (
	id INT unique NOT NULL AUTO_INCREMENT,
    COD_THEMA CHAR(20),
    COD_RUBRICA CHAR(20) unique,
    RUBRICA CHAR(255) unique,
	PRIMARY KEY (id),
);


create table books (
	id INT unique NOT NULL AUTO_INCREMENT,
-- book may have several themes and rubrices
--    COD_THEMA VARCHAR(20),
--    COD_RUBRICA VARCHAR(20),
    
    AUTHOR VARCHAR(255),
    NAME VARCHAR(255),
    SECOND_NAME VARCHAR(255),
    PLACE VARCHAR(63),
    PUBLISHER VARCHAR(255),
    YEAR  VARCHAR(20),
    PAGES  VARCHAR(20),
	COVERS VARCHAR(127),
	FORMAT VARCHAR(127),
    COMMENT  VARCHAR(255),
    PRICE INT,
	DESCRIPTION VARCHAR(255),
    DESCRIPTION_COVERS VARCHAR(255),
    STATE_CONDITION VARCHAR(255), 
    DATA DATE,
    
    STATE INT,
    ID_ALIB INT,
    
    SMALL_IMAGE VARCHAR(255),
    LARGE_IMAGE_1 VARCHAR(255),
    LARGE_IMAGE_2 VARCHAR(255),
    LARGE_IMAGE_3 VARCHAR(255),
   
    STATUS INT,
    PAID BOOLEAN,
	PRIMARY KEY (id)
);
--	drop table book_rubrica;
-- book may have several themes and rubrices
-- soo beetwen books and rubrices exists relation many-to-many
CREATE TABLE book_rubrica (
	book_id      integer,
	thema_id     integer,
	cod_thema    char(20),
	rubrica_id   integer,
	cod_rubrica char(20) 
);

create table book_status (
	id INT unique NO T NULL AUTO_INCREMENT,
	code INT default -1,
	STATUS VARCHAR(32),
	PRIMARY KEY (id, code)
);

insert into book_status (id, code, status) values (1, -1, 'info is not confirms');
insert into book_status (id, code, status) values (2,  0, 'ready to sale');
insert into book_status (id, code, status) values (3,  1, 'is reserved');
insert into book_status (id, code, status) values (4,  2, 'is ordered');
insert into book_status (id, code, status) values (5,  3, 'send');
insert into book_status (id, code, status) values (6,  4, 'receive is confirmed');
insert into book_status (id, code, status) values (7,  5, 'saled and paid');

create table buyers (
	id INT unique NOT NULL AUTO_INCREMENT,
	NAME VARCHAR(127),
    E_MAIL VARCHAR(127),
    TEL VARCHAR(127),
    STATE VARCHAR(127),
	POST_INDEX CHAR(6),
    TOWN VARCHAR(127),
    STREET VARCHAR(127),
    HOUSE VARCHAR(12),
    BUILDING VARCHAR(12),
    FLAT VARCHAR(12),
	PRIMARY KEY (id, E_MAIL)
);


LOAD DATA INFILE '../dB.BackUp/themes.bak'
	INTO TABLE THEMES
	FIELDS TERMINATED BY '\t'
	LINES TERMINATED BY '\n';
  
  
