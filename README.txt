	Детальное описание пользовательского интерфейса см. в User.Manual.doc

Detail programmer's manual see in Programmer.Manual.doc
Detail user's manual in russian language see in User.Manual.doc

ABSTRACT

Application represent example of implementation of "E-shop of rare, antiquire and old books".


FUNCTIONALITY

1. Book's rubricator by thema and rubrica
2. Search biik list by 
	- Author, 
	- Book's Titul name, 
	- 'something like ...', 
	- publiushing year from .. till...,  
	- price from .. till...,  
	- store appeared date,
	- thema, rubrica,
	- sort by ...
3. See detail book's description,
4. Request to buy the book,
5. Send answer to book store administrator,
6. New books during last some days/month.
7. Descriptions pages with Book store rules, actions, links to other rubricators.
8. Send your wishes and request for improvement to administaration

REQUIRENTS

- Java - at lest 7.0
- TomEE EJB server - 7.0/ with EJB light container - at least
- MySQL - at leat 5.1
- JDBC driver - 4.0


BUILD 

If you like 'ALL in ONE' then for this aim provide batch file, that do build, deploy and start server by one command line command. That is:
- Build&RunServer.sh - for Linux environmen
- Build&RunServer.bat - for Window environment

Build project are doing under management of ANT.
For properly install of build process, please, check following property that are setting in build.xml:
- <property name="appname" value="OldBook"/>  - set project name,
- <property name="build.dir" value="build" /> - set build folder. The folder contains all compiled classes and resulting "OldBook.war".


DEPLOY and START

The package provide completed compiled and built project in folder "build".
Importance NOTE! The project compiled under JAVA 8.0 version.

IMPORTANCE NOTE: The build was tested in TomEE server v.7.0 only.
   