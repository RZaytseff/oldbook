<%@ page contentType="text/html; charset=utf-8" language="java"  import="java.sql.*, java.util.*, model.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Найти</title>
<style type="text/css">
<!--
.Contex {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	line-height: 12px;
	font-weight: bold;
	font-variant: normal;
	color: #0066CC;
	text-decoration: none;
}
a:link {
	color: #0099CC;
}
a:visited {
	color: #990000;
}
a:hover {
	color: #CC6600;
}
a:active {
	color: #FF0000;
}
.стиль11 {color: #0066CC; font-size: 12px; }
.стиль12 {font-size: 12px}
.стиль13 {
	color: #0066CC;
	font-weight: bold;
	font-size: 12px;
}
body {
	background-color: #B3FDB0;
	margin: 1px;
	padding: 1px;

}
.стиль22 {font-size: 10; font-weight: bold; color: black; }
.стиль23 {color: black}
.стиль24 {
	color: black;
	font-size: 12px;
	font-weight: bold;
}
.стиль25 {font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; line-height: 12px; font-weight: bold; font-variant: normal; color: black; text-decoration: none; }
-->
</style>
</head>

<body>

    <blockquote>
      <h1 class="стиль25">  Найти книгу: </h1>
    </blockquote>
<NOFOLLOW>
  <form action="ViewForSale.jsp?list=filter" method="get" enctype="multipart/form-data" name="SearchForm" target="mainFrame">
    
  <table width="100%" border="0" cellpadding="0" cellspacing="3" id="SearchTable">
    
  <input type="hidden" name="action" value="search" />
  <input type="hidden" name="list" value="filter" />  


  <tr>
  <td align="right" class="стиль12 стиль11"><span class="стиль22">Автор</span></td>
  <td width="958" class="стиль12"><input name="author" type="text" class="стиль12" style="width: 100%;" value=""></td>
  </tr>
  <tr>
  <td align="right" class="стиль12 стиль11"><span class="стиль22">Название</span></td>
  <td width="958" class="стиль12"><input name="name" type="text" class="стиль12" style="width: 100%;" value=""></td>
  </tr>
    <td align="right" class="стиль12 стиль11"><span class="стиль22">Контекст</span></td>
  <td width="958" class="стиль12"><input name="context" type="text" class="стиль12" style="width: 100%;" value=""></td>
  </tr>
  <tr>
  <td align="right" class="стиль12 стиль11"><span class="стиль22">Год издания</span></td>
  <td width="958" class="стиль12">
  <table border="0" cellspacing="0" cellpadding="5" width="100%">
  <tr>
  <td width="4%" class="search стиль12 стиль23"><strong>от </strong></td>
  <td width="43%"><input name="from_year" type="text" class="стиль13" style="width: 100%;" value="" size="6" maxlength="8"></td>
  <td width="5%" class="search"><div align="right" class="стиль23"><span class="стиль24">до </span></div></td>
  <td width="48%"><input name="to_year" type="text" class="стиль13" style="width: 100%;" value="" size="6"  maxlength="8"></td>
  </tr>
  </table></td>
  </tr>
  <tr>
  <td align="right" class="стиль12 стиль11"><span class="стиль22">Цена</span></td>
  
<td width="958" class="стиль12">
  <table border="0" cellspacing="0" cellpadding="5" width="100%">
  <tr>
  <td width="4%" class="стиль11 стиль12 стиль23"><strong>от </strong></td>  
  <td width="43%"><input name="from_price" type="text" class="стиль13" style="width: 100%;" value="" size="6" maxlength="8"></td>
  <td width="5%" class="стиль13"><div align="right" class="стиль23">до</div></td> 
  <td width="48%"><input name="to_price" type="text" class="стиль13" style="width: 100%;"   value="" size="6" maxlength="8"></td>
  </tr>
  </table></td>
  </tr>
  <tr>
  <td align="right" nowrap class="стиль12 стиль11"><span class="стиль22">Поступления за</span></td>
  <td width="958" class="стиль12"><span class="стиль23" style="font-size: 12px"><strong>
    <select name="last" style="width: 100%;">
      <option value="all" >все время</option>
      <option value="30" >последний месяц</option>
      <option value="6" >последнюю неделю</option>
      <option value="2" >три дня</option>
      <option value="1" >два дня</option>
      <option value="0" >сегодня</option>
    </select>
  </strong></span></td>
  </tr>
  <tr>
  <td align="right" class="стиль12 стиль11"><span class="стиль22">Тема /Рубрика</span></td>
  
  <td width="958" class="стиль12">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="right" class="стиль12 стиль11"><span class="стиль23" style="font-size: 12px"><strong>
      <select name="rubrica" class="стиль12" style="width: 100%;">
        <option value="">все рубрики</option>
        
		<jsp:useBean id="rubricatorBean" class="bean.RubricatorBean" scope="session"/>
		<% List<Rubricator> rubricator = rubricatorBean.getRubricator();
			int themaNumber = 0;
			int size = rubricator.size();
		  	for (Rubricator rubrica: rubricator) { 
				String themaCode =   rubrica.getThemaCode();
				String rubricaCode = rubrica.getRubricaCode();
				String name =        rubrica.getName();
				%>
		        <option value="<%=(rubrica.isThema()  ? "thema:": "rubrica:")%>
		        <%=((rubricaCode!=null)?rubricaCode:"")%>">
		        <%=(rubrica.isThema()  ?"":"----" )%>
		        <%=((name!=null)?name:"")%>
		        </option>
		        <%
			}
		%>
      </select>
    </strong></span></td>
    </tr>
  <tr>
  <td align="right" class="стиль12 стиль11"><span class="стиль22">Сортировать</span></td>
  <td width="958" class="стиль12"><span class="стиль23" style="font-size: 12px"><strong>
    <select name="sort" style="width: 100%;">
      <option value="author" >по автору</option>
      <option value="name, secondName" >по названию</option>
      <option value="price" >по цене по возрастанию</option>
      <option value="price DESC" >по цене по убыванию</option>
      <option value="data DESC" >по дате поступления</option>
      <option value="year" >по году издания</option>
    </select>
  </strong></span></td>
  </tr>
  </table>
  
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
  <td align="right"><input type="image" src="images/search.gif" alt="найти" width="66" height="23" hspace="10" border="0"></td>
  </tr>
  </table>
  </form>
</NOFOLLOW>   
</html>

