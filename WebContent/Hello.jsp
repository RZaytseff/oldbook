<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Привет!</title>
<style type="text/css">
<!--
.стиль1 {
	font-size: 18px;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #990000;
}
.стиль2 {
	font-size: 18px;
	color: #006699;
}
body {
	background-color: #B3FDB0;
	margin: 6px;
	padding: 5px;

}
.стиль5 {font-weight: bold}
-->
</style>
</head>

<body>
<blockquote>
  <h1 class="стиль1">ДОБРО  ПОЖАЛОВАТЬ!</h1>
</blockquote>
<p class="стиль2">Позвольте Вас приветствовать в антикварном интернет-магазине  &quot;Редкие <span class="стиль2">антикварные</span> книги&quot;.   </p>
<p class="стиль2">Принимаются заказы  как на книги, выставленные на продажу в магазине, так и на поиск  книг и формирование библиотек на заданную тему.</p>
<p class="стиль2">На этом сайте представлены только наиболее редкие и ценные книги.</p>
<p class="стиль2">Все книги можно посмотреть и заказать на сайте Alib.ru, смотрите три каталога:</p>
<p><span class="стиль5"><a href="http://www.alib.ru/bs.php4?bs=piteroldbook">Каталог книг 1</a></span></p>
<p><strong><a href="http://www.alib.ru/bs.php4?bs=Exclusive">Каталог книг 2</a></strong></p>
<p><strong><a href="http://www.alib.ru/bs.php4?bs=andanilov">Каталог книг 3</a></strong></p>
<p class="стиль2"><a href="http://www.piteroldbook.ru">PiterOldBook.ru</a> является участником Антикварного Салона в ЦДХ с 18 по 26 октября 2008 г. На Салоне Вы можете ознакомиться с лучшими из наших книг.</p>
<p class="стиль2"><a href="Wishes.jsp" target="mainFrame">Ваши замечания и пожелания</a> по работе магазина будут с удовольствием учтены. </p>
</body>

</html>
