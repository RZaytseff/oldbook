<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="Connections/oldbooks.jsp" %>
<%
// *** Edit Operations: declare variables

// set the form action variable
String MM_editAction = request.getRequestURI();
if (request.getQueryString() != null && request.getQueryString().length() > 0) {
  String queryString = request.getQueryString();
  String tempStr = "";
  for (int i=0; i < queryString.length(); i++) {
    if (queryString.charAt(i) == '<') tempStr = tempStr + "&lt;";
    else if (queryString.charAt(i) == '>') tempStr = tempStr + "&gt;";
    else if (queryString.charAt(i) == '"') tempStr = tempStr +  "&quot;";
    else tempStr = tempStr + queryString.charAt(i);
  }
  MM_editAction += "?" + tempStr;
}

// connection information
String MM_editDriver = null, MM_editConnection = null, MM_editUserName = null, MM_editPassword = null;

// redirect information
String MM_editRedirectUrl = null;

// query string to execute
StringBuffer MM_editQuery = null;

// boolean to abort record edit
boolean MM_abortEdit = false;

// table information
String MM_editTable = null, MM_editColumn = null, MM_recordId = null;

// form field information
String[] MM_fields = null, MM_columns = null;
%>
<%
// *** Insert Record: set variables

if (request.getParameter("MM_insert") != null && request.getParameter("MM_insert").toString().equals("form1")) {

  MM_editDriver     = MM_oldbooks_DRIVER;
  MM_editConnection = MM_oldbooks_STRING;
  MM_editUserName   = MM_oldbooks_USERNAME;
  MM_editPassword   = MM_oldbooks_PASSWORD;
  MM_editTable  = " books";
  MM_editRedirectUrl = "view.jsp";
  String MM_fieldsStr = "ID|value|AUTOR|value|NAME|value|SECOND_NAME|value|PLACE|value|PUBLISHER|value|YEAR|value|PAGES|value|COVERS|value|FORMAT|value|DESCRIPTION|value|CONDITION|value|DATA|value|PRICE|value|ID_ALIB|value|SMALL_IMAGE|value|LARGE_IMAGE_1|value|LARGE_IMAGE_2|value|LARGE_IMAGE_3|value";
  String MM_columnsStr = "ID|none,none,NULL|AUTOR|',none,''|NAME|',none,''|SECOND_NAME|',none,''|PLACE|',none,''|PUBLISHER|',none,''|`YEAR`|',none,''|PAGES|',none,''|COVERS|',none,''|FORMAT|',none,''|`DESCRIPTION`|',none,''|CONDITION|',none,''|`DATA`|',none,NULL|PRICE|none,none,NULL|ID_ALIB|',none,''|SMALL_IMAGE|none,none,NULL|LARGE_IMAGE_1|none,none,NULL|LARGE_IMAGE_2|none,none,NULL|LARGE_IMAGE_3|none,none,NULL";

  // create the MM_fields and MM_columns arrays
  java.util.StringTokenizer tokens = new java.util.StringTokenizer(MM_fieldsStr,"|");
  MM_fields = new String[tokens.countTokens()];
  for (int i=0; tokens.hasMoreTokens(); i++) MM_fields[i] = tokens.nextToken();

  tokens = new java.util.StringTokenizer(MM_columnsStr,"|");
  MM_columns = new String[tokens.countTokens()];
  for (int i=0; tokens.hasMoreTokens(); i++) MM_columns[i] = tokens.nextToken();

  // set the form values
  for (int i=0; i+1 < MM_fields.length; i+=2) {
    MM_fields[i+1] = ((request.getParameter(MM_fields[i])!=null)?(String)request.getParameter(MM_fields[i]):"");
  }
 
  // append the query string to the redirect URL
  if (MM_editRedirectUrl.length() != 0 && request.getQueryString() != null) {
    MM_editRedirectUrl += ((MM_editRedirectUrl.indexOf('?') == -1)?"?":"&") + request.getQueryString();
  }
}
%>
<%
// *** Insert Record: construct a sql insert statement and execute it

if (request.getParameter("MM_insert") != null) {

  // create the insert sql statement
  StringBuffer MM_tableValues = new StringBuffer(), MM_dbValues = new StringBuffer();
  String[] MM_dbValues_prep = new String[MM_fields.length/2];
  for (int i=0; i+1 < MM_fields.length; i+=2) {
    String formVal = MM_fields[i+1];
    String elem;
    java.util.StringTokenizer tokens = new java.util.StringTokenizer(MM_columns[i+1],",");
    elem = (String)tokens.nextToken(); // consume the delim
    String altVal   = ((elem = (String)tokens.nextToken()) != null && elem.compareTo("none")!=0)?elem:"";
    String emptyVal = ((elem = (String)tokens.nextToken()) != null && elem.compareTo("none")!=0)?elem:"";
    if (formVal.length() == 0) {
        if(emptyVal.equals("NULL")) {
            formVal = null;
        } else if(emptyVal.charAt(0) == '\'') {
            formVal = emptyVal.substring(1, emptyVal.length()-1);
        } else {
            formVal = emptyVal;
        }
    } else if (altVal.length() != 0) {
        if(altVal.charAt(0) == '\'') {
            formVal = altVal.substring(1, altVal.length()-1);
        } else {
            formVal = altVal;
        }
    }
    MM_dbValues_prep[i/2] = formVal;
    MM_tableValues.append((i!=0)?",":"").append(MM_columns[i]);
    MM_dbValues.append((i!=0)?",":"").append('?');
  }
  MM_editQuery = new StringBuffer("insert into " + MM_editTable);
  MM_editQuery.append(" (").append(MM_tableValues.toString()).append(") values (");
  MM_editQuery.append(MM_dbValues.toString()).append(")");
  
  if (!MM_abortEdit) {
    // finish the sql and execute it
    Driver MM_driver = (Driver)Class.forName(MM_editDriver).newInstance();
    Connection MM_connection = DriverManager.getConnection(MM_editConnection,MM_editUserName,MM_editPassword);
    PreparedStatement MM_editStatement = MM_connection.prepareStatement(MM_editQuery.toString());
    for(int i=0; i<MM_dbValues_prep.length; i++) {
        MM_editStatement.setObject(i+1, MM_dbValues_prep[i]);
    }
    MM_editStatement.executeUpdate();
    MM_connection.close();

    // redirect with URL parameters
    if (MM_editRedirectUrl.length() != 0) {
      response.sendRedirect(response.encodeRedirectURL(MM_editRedirectUrl));
      return;
    }
  }
}
%>
<%
Driver DriverADD = (Driver)Class.forName(MM_oldbooks_DRIVER).newInstance();
Connection ConnADD = DriverManager.getConnection(MM_oldbooks_STRING,MM_oldbooks_USERNAME,MM_oldbooks_PASSWORD);
PreparedStatement ADD = ConnADD.prepareStatement("INSERT INTO  books (AUTOR, NAME, SECOND_NAME, PLACE, PUBLISHER, YEAR, PAGES, COVERS, FORMAT, DESCRIPTION, CONDITION, DATA, PRICE, ID_ALIB, SMALL_IMAGE, LARGE_IMAGE_1, LARGE_IMAGE_2, LARGE_IMAGE_3)  VALUES ( ) ");
ADD.executeUpdate();
%>
<%
Driver DriverADDing = (Driver)Class.forName(MM_oldbooks_DRIVER).newInstance();
Connection ConnADDing = DriverManager.getConnection(MM_oldbooks_STRING,MM_oldbooks_USERNAME,MM_oldbooks_PASSWORD);
PreparedStatement StatementADDing = ConnADDing.prepareStatement("SELECT * FROM  books");
ResultSet ADDing = StatementADDing.executeQuery();
boolean ADDing_isEmpty = !ADDing.next();
boolean ADDing_hasData = !ADDing_isEmpty;
Object ADDing_data;
int ADDing_numRows = 0;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Новая книга</title>
<style type="text/css">
<!--
.стиль1 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 36px;
	font-weight: bold;
	color: #336600;
}
body {
	background-color: #FFFF00;
}
-->
</style>
</head>

<body>
<div align="center"><span class="стиль1">Новая книга</span></div>
<form action="<%=MM_editAction%>" method="post" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">ID:</td>
      <td><input type="text" name="ID" value="<%=(((ADDing_data = ADDing.getObject(""))==null || ADDing.wasNull())?"":ADDing_data)%>" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">AUTOR:</td>
      <td><input type="text" name="AUTOR" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">NAME:</td>
      <td><input type="text" name="NAME" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">SECOND_NAME:</td>
      <td><input type="text" name="SECOND_NAME" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">PLACE:</td>
      <td><input type="text" name="PLACE" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">PUBLISHER:</td>
      <td><input type="text" name="PUBLISHER" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">YEAR:</td>
      <td><input type="text" name="YEAR" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">PAGES:</td>
      <td><input type="text" name="PAGES" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">COVERS:</td>
      <td><input type="text" name="COVERS" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">FORMAT:</td>
      <td><input type="text" name="FORMAT" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">DESCRIPTION:</td>
      <td><input type="text" name="DESCRIPTION" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">CONDITION:</td>
      <td><input type="text" name="CONDITION" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">DATA:</td>
      <td><input type="text" name="DATA" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">PRICE:</td>
      <td><input type="text" name="PRICE" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">ID_ALIB:</td>
      <td><input type="text" name="ID_ALIB" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">SMALL_IMAGE:</td>
      <td><input type="text" name="SMALL_IMAGE" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">LARGE_IMAGE_1:</td>
      <td><input type="text" name="LARGE_IMAGE_1" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">LARGE_IMAGE_2:</td>
      <td><input type="text" name="LARGE_IMAGE_2" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">LARGE_IMAGE_3:</td>
      <td><input type="text" name="LARGE_IMAGE_3" value="" size="32" />
      </td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><pre><input type="submit" class="стиль1" value="Выставить книгу на продажу" />       
      </pre></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>
<p>&nbsp;</p>
</body>
</html>
<%
ADDing.close();
StatementADDing.close();
ConnADDing.close();
%>
<%
ConnADD.close();
%>
