<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="Connections/oldbooks.jsp" %>
<%
Driver Driverbooks = (Driver)Class.forName(MM_oldbooks_DRIVER).newInstance();
Connection Connbooks = DriverManager.getConnection(MM_oldbooks_STRING,MM_oldbooks_USERNAME,MM_oldbooks_PASSWORD);
PreparedStatement Statementbooks = Connbooks.prepareStatement("SELECT * FROM oldbooks.books");
ResultSet books = Statementbooks.executeQuery();
boolean books_isEmpty = !books.next();
boolean books_hasData = !books_isEmpty;
Object books_data;
int books_numRows = 0;
%>
<%
int Repeat1__numRows = 10;
int Repeat1__index = 0;
books_numRows += Repeat1__numRows;
%>
<%
// *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

int books_first = 1;
int books_last  = 1;
int books_total = -1;

if (books_isEmpty) {
  books_total = books_first = books_last = 0;
}

//set the number of rows displayed on this page
if (books_numRows == 0) {
  books_numRows = 1;
}
%>
<%
// *** Recordset Stats: if we don't know the record count, manually count them

if (books_total == -1) {

  // count the total records by iterating through the recordset
    for (books_total = 1; books.next(); books_total++);

  // reset the cursor to the beginning
  books.close();
  books = Statementbooks.executeQuery();
  books_hasData = books.next();

  // set the number of rows displayed on this page
  if (books_numRows < 0 || books_numRows > books_total) {
    books_numRows = books_total;
  }

  // set the first and last displayed record
  books_first = Math.min(books_first, books_total);
  books_last  = Math.min(books_first + books_numRows - 1, books_total);
}
%>
<% String MM_paramName = ""; %>
<%
// *** Move To Record and Go To Record: declare variables

ResultSet MM_rs = books;
int       MM_rsCount = books_total;
int       MM_size = books_numRows;
String    MM_uniqueCol = "";
          MM_paramName = "";
int       MM_offset = 0;
boolean   MM_atTotal = false;
boolean   MM_paramIsDefined = (MM_paramName.length() != 0 && request.getParameter(MM_paramName) != null);
%>
<%
// *** Move To Record: handle 'index' or 'offset' parameter

if (!MM_paramIsDefined && MM_rsCount != 0) {

  //use index parameter if defined, otherwise use offset parameter
  String r = request.getParameter("index");
  if (r==null) r = request.getParameter("offset");
  if (r!=null) MM_offset = Integer.parseInt(r);

  // if we have a record count, check if we are past the end of the recordset
  if (MM_rsCount != -1) {
    if (MM_offset >= MM_rsCount || MM_offset == -1) {  // past end or move last
      if (MM_rsCount % MM_size != 0)    // last page not a full repeat region
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  //move the cursor to the selected record
  int i;
  for (i=0; books_hasData && (i < MM_offset || MM_offset == -1); i++) {
    books_hasData = MM_rs.next();
  }
  if (!books_hasData) MM_offset = i;  // set MM_offset to the last possible record
}
%>
<%
// *** Move To Record: if we dont know the record count, check the display range

if (MM_rsCount == -1) {

  // walk to the end of the display range for this page
  int i;
  for (i=MM_offset; books_hasData && (MM_size < 0 || i < MM_offset + MM_size); i++) {
    books_hasData = MM_rs.next();
  }

  // if we walked off the end of the recordset, set MM_rsCount and MM_size
  if (!books_hasData) {
    MM_rsCount = i;
    if (MM_size < 0 || MM_size > MM_rsCount) MM_size = MM_rsCount;
  }

  // if we walked off the end, set the offset based on page size
  if (!books_hasData && !MM_paramIsDefined) {
    if (MM_offset > MM_rsCount - MM_size || MM_offset == -1) { //check if past end or last
      if (MM_rsCount % MM_size != 0)  //last page has less records than MM_size
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  // reset the cursor to the beginning
  books.close();
  books = Statementbooks.executeQuery();
  books_hasData = books.next();
  MM_rs = books;

  // move the cursor to the selected record
  for (i=0; books_hasData && i < MM_offset; i++) {
    books_hasData = MM_rs.next();
  }
}
%>
<%
// *** Move To Record: update recordset stats

// set the first and last displayed record
books_first = MM_offset + 1;
books_last  = MM_offset + MM_size;
if (MM_rsCount != -1) {
  books_first = Math.min(books_first, MM_rsCount);
  books_last  = Math.min(books_last, MM_rsCount);
}

// set the boolean used by hide region to check if we are on the last record
MM_atTotal  = (MM_rsCount != -1 && MM_offset + MM_size >= MM_rsCount);
%>
<%
// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

String MM_keepBoth,MM_keepURL="",MM_keepForm="",MM_keepNone="";
String[] MM_removeList = { "index", MM_paramName };

// create the MM_keepURL string
if (request.getQueryString() != null) {
  MM_keepURL = '&' + request.getQueryString();
  for (int i=0; i < MM_removeList.length && MM_removeList[i].length() != 0; i++) {
  int start = MM_keepURL.indexOf(MM_removeList[i]) - 1;
    if (start >= 0 && MM_keepURL.charAt(start) == '&' &&
        MM_keepURL.charAt(start + MM_removeList[i].length() + 1) == '=') {
      int stop = MM_keepURL.indexOf('&', start + 1);
      if (stop == -1) stop = MM_keepURL.length();
      MM_keepURL = MM_keepURL.substring(0,start) + MM_keepURL.substring(stop);
    }
  }
}

// add the Form variables to the MM_keepForm string
if (request.getParameterNames().hasMoreElements()) {
  java.util.Enumeration items = request.getParameterNames();
  while (items.hasMoreElements()) {
    String nextItem = (String)items.nextElement();
    boolean found = false;
    for (int i=0; !found && i < MM_removeList.length; i++) {
      if (MM_removeList[i].equals(nextItem)) found = true;
    }
    if (!found && MM_keepURL.indexOf('&' + nextItem + '=') == -1) {
      MM_keepForm = MM_keepForm + '&' + nextItem + '=' + java.net.URLEncoder.encode(request.getParameter(nextItem));
    }
  }
}

String tempStr = "";
for (int i=0; i < MM_keepURL.length(); i++) {
  if (MM_keepURL.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepURL.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepURL.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepURL.charAt(i);
}
MM_keepURL = tempStr;

tempStr = "";
for (int i=0; i < MM_keepForm.length(); i++) {
  if (MM_keepForm.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepForm.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepForm.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepForm.charAt(i);
}
MM_keepForm = tempStr;

// create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL + MM_keepForm;
if (MM_keepBoth.length() > 0) MM_keepBoth = MM_keepBoth.substring(1);
if (MM_keepURL.length() > 0)  MM_keepURL = MM_keepURL.substring(1);
if (MM_keepForm.length() > 0) MM_keepForm = MM_keepForm.substring(1);
%>
<%
// *** Move To Record: set the strings for the first, last, next, and previous links

String MM_moveFirst,MM_moveLast,MM_moveNext,MM_movePrev;
{
  String MM_keepMove = MM_keepBoth;  // keep both Form and URL parameters for moves
  String MM_moveParam = "index=";

  // if the page has a repeated region, remove 'offset' from the maintained parameters
  if (MM_size > 1) {
    MM_moveParam = "offset=";
    int start = MM_keepMove.indexOf(MM_moveParam);
    if (start != -1 && (start == 0 || MM_keepMove.charAt(start-1) == '&')) {
      int stop = MM_keepMove.indexOf('&', start);
      if (start == 0 && stop != -1) stop++;
      if (stop == -1) stop = MM_keepMove.length();
      if (start > 0) start--;
      MM_keepMove = MM_keepMove.substring(0,start) + MM_keepMove.substring(stop);
    }
  }

  // set the strings for the move to links
  StringBuffer urlStr = new StringBuffer(request.getRequestURI()).append('?').append(MM_keepMove);
  if (MM_keepMove.length() > 0) urlStr.append('&');
  urlStr.append(MM_moveParam);
  MM_moveFirst = urlStr + "0";
  MM_moveLast  = urlStr + "-1";
  MM_moveNext  = urlStr + Integer.toString(MM_offset+MM_size);
  MM_movePrev  = urlStr + Integer.toString(Math.max(MM_offset-MM_size,0));
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Редкие книги</title>
</head>

<body>
<table border="1">
  <tr>
    <td>ID</td>
    <td>THEMA</td>
    <td>RUBRICA</td>
    <td>AUTHOR</td>
    <td>NAME</td>
    <td>SECOND_NAME</td>
    <td>PLACE</td>
    <td>PUBLISHER</td>
    <td>YEAR</td>
    <td>PAGES</td>
    <td>COVERS</td>
    <td>FORMAT</td>
    <td>EXTENSION</td>
    <td>PRICE</td>
    <td>PRICE_BUY</td>
    <td>DESCRIPTION</td>
    <td>CONDITION</td>
    <td>DATA</td>
    <td>DESCRIPTION_COVERS</td>
    <td>SHELF</td>
    <td>STATUS</td>
    <td>SMALL_IMAGE</td>
    <td>LARGE_IMAGE_1</td>
    <td>LARGE_IMAGE_2</td>
    <td>LARGE_IMAGE_3</td>
    <td>ID_ALIB</td>
    <td>DATA_ORDER</td>
    <td>DATA_SALE</td>
    <td>ID_BUYER</td>
    <td>PRICE_SALE</td>
    <td>COMMENT_SALE</td>
  </tr>
  <% while ((books_hasData)&&(Repeat1__numRows-- != 0)) { %>
    <tr>
      <td><%=(((books_data = books.getObject("ID"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("THEMA"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("RUBRICA"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("AUTHOR"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("NAME"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("SECOND_NAME"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("PLACE"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("PUBLISHER"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("YEAR"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("PAGES"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("COVERS"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("FORMAT"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("EXTENSION"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("PRICE"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("PRICE_BUY"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("DESCRIPTION"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("CONDITION"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("DATA"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("DESCRIPTION_COVERS"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("SHELF"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("STATUS"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("SMALL_IMAGE"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("LARGE_IMAGE_1"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("LARGE_IMAGE_2"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("LARGE_IMAGE_3"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("ID_ALIB"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("DATA_ORDER"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("DATA_SALE"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("ID_BUYER"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("PRICE_SALE"))==null || books.wasNull())?"":books_data)%></td>
      <td><%=(((books_data = books.getObject("COMMENT_SALE"))==null || books.wasNull())?"":books_data)%></td>
    </tr>
    <%
  Repeat1__index++;
  books_hasData = books.next();
}
%>
</table>
<p>&nbsp;</p>

<table border="0">
  <tr>
    <td><% if (MM_offset !=0) { %>
          <a href="<%=MM_moveFirst%>"><img src="images/First.gif" border=0></a>
          <% } /* end MM_offset != 0 */ %>
    </td>
    <td><% if (MM_offset !=0) { %>
          <a href="<%=MM_movePrev%>"><img src="images/Previous.gif" border=0></a>
          <% } /* end MM_offset != 0 */ %>
    </td>
    
    <td><% if (!MM_atTotal) { %>
          <a href="<%=MM_moveNext%>"><img src="images/Next.gif" border=0></a>
          <% } /* end !MM_atTotal */ %>
    </td>
    <td><% if (!MM_atTotal) { %>
          <a href="<%=MM_moveLast%>"><img src="images/Last.gif" border=0> </a>
          <% } /* end !MM_atTotal */ %>
          
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Записи с <%=(books_first)%> по  <%=(books_last)%> <%=(books_total)%> 
    
    </td>
  </tr>
</table>
</body>
</html>
<%
books.close();
Statementbooks.close();
Connbooks.close();
%>
