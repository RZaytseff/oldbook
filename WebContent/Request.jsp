<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.util.Properties" %>
<%@ page import="javax.mail.internet.AddressException, javax.mail.internet.MimeMessage, javax.mail.internet.InternetAddress" %>
<%@ page import="javax.mail.Session, javax.mail.MessagingException, javax.mail.Message, javax.mail.Transport" %>
<%@ page import="javax.mail.Authenticator, javax.mail.PasswordAuthentication" %>

<%@ include file="Connections/oldbooks.jsp" %>
<%@ include file="Connections/mail.properties.jsp" %>
<%
String id = "0";
if (request.getParameter("id")!=null) id = request.getParameter("id");

Driver Driverbook = (Driver)Class.forName(MM_oldbooks_DRIVER).newInstance();
Connection Connbook = DriverManager.getConnection(MM_oldbooks_STRING,MM_oldbooks_USERNAME,MM_oldbooks_PASSWORD);
PreparedStatement Statementbook = Connbook.prepareStatement("SELECT * FROM oldbooks.books WHERE ID='" + id + "'");
ResultSet books = Statementbook.executeQuery();
boolean book_isEmpty = !books.next();
boolean book_hasData = !book_isEmpty;
Object books_data;
int book_numRows = 0;
%>

<%
String orderRequest = "";
String orderContent = "";

orderContent+="Покупатель: " + (new String(request.getParameter("FIO").getBytes("ISO-8859-1"),"utf-8")) + " \n";
orderContent+="e-mail: " + (new String(request.getParameter("E_MAIL").getBytes("ISO-8859-1"),"utf-8")) + " \n";
orderContent+="телефон: " + (new String(request.getParameter("TEL").getBytes("ISO-8859-1"),"utf-8")) + " \n";
orderContent+="книга: " + 
		(((books_data = books.getObject("AUTHOR"))==null || books.wasNull())?"":books_data).toString() + " " +
		(((books_data = books.getObject("NAME"))==null || books.wasNull())?"":books_data).toString() + " " +
		(((books_data = books.getObject("SECOND_NAME"))==null || books.wasNull())?"":books_data).toString() +
		(((books_data = books.getObject("PLACE"))==null || books.wasNull())?"":books_data).toString() + " " +
		(((books_data = books.getObject("PUBLISHER"))==null || books.wasNull())?"":books_data).toString() + ". " + " " +
		(((books_data = books.getObject("YEAR"))==null || books.wasNull())?"":books_data).toString() + ". "  + "\n" +
		(((books_data = books.getObject("COVERS"))==null || books.wasNull())?"":books_data).toString() + ". "  +  " " +
		(((books_data = books.getObject("FORMAT"))==null || books.wasNull())?"":books_data).toString() + ". "  +  " " +
		(((books_data = books.getObject("DESCRIPTION_COVERS"))==null || books.wasNull())?"":books_data).toString() + ". "  +  "\n" +
		"Цена: " +
		(((books_data = books.getObject("PRICE"))==null || books.wasNull())?"":books_data).toString() +  "\n"  +  "\n" ;

orderContent+="Адрес покупателя: " + "\n"  +
		"Страна: "  + (new String(request.getParameter("STATE").getBytes("ISO-8859-1"),"utf-8")) + ", " + "\n"  +
		"индекс: "  + (new String(request.getParameter("INDEX").getBytes("ISO-8859-1"),"utf-8")) + ", " + "\n"  +
		"город: "   + (new String(request.getParameter("TOWN").getBytes("ISO-8859-1"),"utf-8")) + ", " + "\n"  +
		"улица: "   + (new String(request.getParameter("STREET").getBytes("ISO-8859-1"),"utf-8")) + ", " + "\n"  +
		"дом: "     + (new String(request.getParameter("HOUSE").getBytes("ISO-8859-1"),"utf-8")) + ", " + 
		"корпус: "  + (new String(request.getParameter("BUILDING").getBytes("ISO-8859-1"),"utf-8")) + ", "  + 
		"корпус: "  + (new String(request.getParameter("FLAT").getBytes("ISO-8859-1"),"utf-8"));
		
		
%>
<%

//request.setAttribute("username",username);
//request.setAttribute("password",password);

	class SMTPAuthenticator extends javax.mail.Authenticator {
		private String userName="username", passWord="password";
		public void setUserName(String username) { this.userName = username; }
		public void setPassword(String password) { this.passWord = password; }
        public PasswordAuthentication getPasswordAuthentication() {
           return new PasswordAuthentication("PiterOldBook", "ADanilov");
        }
    }

 
    Properties props = System.getProperties();
    props.setProperty("mail.smtp.host", smtpHost);
    props.setProperty("mail.smtp.port", ""+smtpPort);
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
try {
    Authenticator auth = new SMTPAuthenticator();
//	out.println(username + "   " + password);
//	auth.UserName=username;
//	auth.setPassword(password);
    Session mailSession = Session.getInstance(props, auth);
    mailSession.setDebug(true);
    Transport transport = mailSession.getTransport();

// Construct the message
    Message msg = new MimeMessage(mailSession);
    msg.setFrom(new InternetAddress(from));
    msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
    msg.setSubject(new String(subjectOrder.getBytes("ISO-8859-1"),"utf-8"));
    msg.setText(orderContent);
//    Transport.send(msg);
    transport.connect();
    transport.sendMessage(msg, msg.getRecipients(Message.RecipientType.TO));
    transport.close();

	
	Statementbook = Connbook.prepareStatement("UPDATE oldbooks.books SET STATUS=2 WHERE ID='" + id + "'");
	Statementbook.executeUpdate();
		
		out.println("<h2 align='center'>Ваш заказ оформлен</h2>");
		out.println("<h3 align='center'>Если в течении суток Вы не получите подтверждения о заказе, вероятнее всего Вы неправильно указали свой e-mail.</h3>");		
} catch (Exception e)
      {
	    out.println("<h2 align='center'>При передаче сведений о заказе произошла техническая ошибка !!!</h2>");
        out.println("" + e);
      }

	
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Заказ книги</title>
<style type="text/css">
<!--
body,td,th {
	color: #000000;
}
body {
	background-color: #B3FDB0;
}
a:link {
	color: #003366;
}
a:visited {
	color: #993300;
}
a:hover {
	color: #CC3333;
}
a:active {
	color: #FF0000;
}
.стиль1 {
	font-size: 16px;
	font-weight: bold;
}
-->
</style>

</head>

<body>
	<br/>
<% 
	String history = "1";
	String fromPage = "";
	if (request.getParameter("fromPage")!=null) fromPage = request.getParameter("fromPage");
	if (fromPage.equals("View"))   history = "2";
	if (fromPage.equals("Detail")) history = "3";
//	out.println("history = " + history);
%>
    
    <p align="center">
        <a href="javascript:history.go(-<%=history%>);" target="mainFrame">Вернуться к списку книг</a>	
    </p>

</body>
</html>
<%
books.close();
Statementbook.close();
Connbook.close();
%>
