<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="Connections/oldbooks.jsp" %>

<%
Driver DriverLatestBooks = (Driver)Class.forName(MM_oldbooks_DRIVER).newInstance();
Connection ConnLatestBooks = DriverManager.getConnection(MM_oldbooks_STRING,MM_oldbooks_USERNAME,MM_oldbooks_PASSWORD);

PreparedStatement StatementLatestBooks = ConnLatestBooks.prepareStatement("SELECT COUNT(*) AS total_count FROM  books WHERE (STATUS is NULL or STATUS = 0 or STATUS = 1) ");
ResultSet LatestBooks = StatementLatestBooks.executeQuery();

int books_total = -1;

if (!LatestBooks.next()) {
  books_total = 0;
} else {
//System.err.println("books_total = " + LatestBooks.getObject("total_count"));
books_total = Integer.parseInt(LatestBooks.getObject("total_count").toString()); 
}

  // reset the cursor to the beginning
  LatestBooks.close();
  StatementLatestBooks = ConnLatestBooks.prepareStatement
  			("SELECT COUNT(*) AS total_count FROM  books where  CURDATE()<= DATA AND (STATUS is NULL or STATUS = 0 or STATUS = 1)");  
  LatestBooks = StatementLatestBooks.executeQuery();

int LastDayBooks_total = -1;

if (!LatestBooks.next()) {
  LastDayBooks_total = 0;
} else {
//System.err.println("LastDayBooks_total = " + LatestBooks.getObject("total_count"));
LastDayBooks_total = Integer.parseInt(LatestBooks.getObject("total_count").toString()); 
}

  // reset the cursor to the beginning
  LatestBooks.close();
  StatementLatestBooks = ConnLatestBooks.prepareStatement
  			("SELECT COUNT(*) AS total_count FROM  books where  DATE_SUB(CURDATE(), INTERVAL 1 DAY) <= DATA AND (STATUS is NULL or STATUS = 0 or STATUS = 1)");  
  LatestBooks = StatementLatestBooks.executeQuery();

int Last2DaysBooks_total = -1;

if (!LatestBooks.next()) {
  Last2DaysBooks_total = 0;
} else {
//System.err.println("LastDayBooks_total = " + LatestBooks.getObject("total_count"));
Last2DaysBooks_total = Integer.parseInt(LatestBooks.getObject("total_count").toString()); 
}

  // reset the cursor to the beginning
  LatestBooks.close();
  StatementLatestBooks = ConnLatestBooks.prepareStatement
  			("SELECT COUNT(*) AS total_count FROM  books where  DATE_SUB(CURDATE(), INTERVAL 2 DAY) <= DATA AND (STATUS is NULL or STATUS = 0 or STATUS = 1)");  
  LatestBooks = StatementLatestBooks.executeQuery();

int Last3DaysBooks_total = -1;

if (!LatestBooks.next()) {
  Last3DaysBooks_total = 0;
} else {
//System.err.println("LastDayBooks_total = " + LatestBooks.getObject("total_count"));
Last3DaysBooks_total = Integer.parseInt(LatestBooks.getObject("total_count").toString()); 
}

  // reset the cursor to the beginning
  LatestBooks.close();
  StatementLatestBooks = ConnLatestBooks.prepareStatement
  			("SELECT COUNT(*) AS total_count FROM  books where  DATE_SUB(CURDATE(), INTERVAL 6 DAY) <= DATA AND (STATUS is NULL or STATUS = 0 or STATUS = 1)");  
  LatestBooks = StatementLatestBooks.executeQuery();

int LastWeekBooks_total = -1;

if (!LatestBooks.next()) {
  LastWeekBooks_total = 0;
} else {
//System.err.println("LastDayBooks_total = " + LatestBooks.getObject("total_count"));
LastWeekBooks_total = Integer.parseInt(LatestBooks.getObject("total_count").toString()); 
}

  // reset the cursor to the beginning
  LatestBooks.close();
  StatementLatestBooks = ConnLatestBooks.prepareStatement
  			("SELECT COUNT(*) AS total_count FROM  books where  DATE_SUB(CURDATE(), INTERVAL 29 DAY) <= DATA AND (STATUS is NULL or STATUS = 0 or STATUS = 1)");  
  LatestBooks = StatementLatestBooks.executeQuery();

int LastMonceBooks_total = -1;

//LatestBooks.next();
if (!LatestBooks.next()) {
  LastMonceBooks_total = 0;
} else {
//System.err.println("LastDayBooks_total = " + LatestBooks.getObject("total_count"));
LastMonceBooks_total = Integer.parseInt(LatestBooks.getObject("total_count").toString()); 
}

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Новинки</title>
<style type="text/css">
<!--

a:link {
	color: #000000;
}
a:visited {
	color: #990000;
}
a:hover {
	color: #CC6600;
}
a:active {
	color: #FF0000;
}
.стиль6 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	line-height: 16px;
	font-weight: bold;
	font-variant: normal;
	color: black;
	text-decoration: none;
}

body {
	background-color: #B3FDB0;
	margin: 1px;
	padding: 1px;

}

.стиль18 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: italic;
	line-height: 22px;
	font-weight: bold;
	font-variant: normal;
	color: black;
	text-decoration: none;
}

-->
</style>
</head>

<body>

  <p class="стиль6">&nbsp;Новинки:</p>
			<blockquote>
              <p class="стиль18">
              <a href="ViewForSale.jsp?last=0"  title="ViewLast" target="mainFrame">сегодня   (<%=LastDayBooks_total%>) </a>
              <br/>
              <a href="ViewForSale.jsp?last=1"  title="ViewLast" target="mainFrame">2 дня   (<%=Last2DaysBooks_total%>) </a>
              <br/>
              <a href="ViewForSale.jsp?last=2"  title="ViewLast" target="mainFrame">3 дня   (<%=Last3DaysBooks_total%>) </a>
              <br/>
              <a href="ViewForSale.jsp?last=6"  title="ViewLast" target="mainFrame">неделя  (<%=LastWeekBooks_total%>) </a>
              <br/>
              <a href="ViewForSale.jsp?last=30" title="ViewLast" target="mainFrame">месяц   (<%=LastMonceBooks_total%>) </a>
              <br/>
              </p>
            </blockquote>
</body>
</html>
