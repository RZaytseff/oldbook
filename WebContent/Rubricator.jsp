﻿<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, java.util.*, model.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Рубрикатор</title>
<style type="text/css">
<!--
.стиль2 {	font-size: x-small
}
.Contex {	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	line-height: 18px;
	font-weight: bold;
	font-variant: normal;
	color: #0066CC;
	text-decoration: none;
}
.стиль7 {	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: italic;
	line-height: 16px;
	font-weight: bold;
	font-variant: normal;
	color: #0066CC;
	text-decoration: none;
}
.стиль8 {font-size: 12px; }
-->
</style>
<script src="SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<link href="SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css" />

<style type="text/css">
<!--
body {
	background-color: #8BFC85;
	margin: 0px;
	padding: 0px;

}
-->
</style></head>


<body>
<jsp:useBean id="rubricatorBean" class="bean.RubricatorBean" scope="session"/>
<% List<Rubricator> rubricator = rubricatorBean.getRubricator(); 
	int themaNumber = -1;
	ListIterator iterator = rubricator.listIterator();
	int size = rubricator.size();
  	while (iterator.hasNext()) {
  		Rubricator rubrica = (Rubricator)iterator.next();
  		themaNumber++;
  		String themaCode = rubrica.getThemaCode();
  		String rubricaCode = rubrica.getRubricaCode(); 
		String name = rubrica.getName();
		int countThema = rubrica.getCountThema();
		int countRubrica = rubrica.getCountRubrica();
		boolean isThema = rubrica.isThema();
		boolean hasRubrices = rubrica.hasRubrices();      
%>

<div id="CollapsiblePanel<%=(++themaNumber)%>" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0">
    <table width="100%" border="0">
      <tr>
        <td align="left">  
        		<a href="ViewForSale.jsp?list=thema&rubrica=thema:<%=themaCode%>" target="mainFrame">
                        <%= name + " (" + countThema + ")" %>
        		</a>
        </td>
        <td align="right"> 
          &nbsp;   
            <% if (isThema && hasRubrices) { %>
                <img name="CollapsibleThema<%=(themaNumber)%>"
                     src="images/Next.gif" alt="Рубрики" width="10" height="8" align="bottom" />
            <% } %>
         </td>
      </tr>
    </table>
  </div>
  
	<% if (isThema && hasRubrices  && iterator.hasNext()) { 
		rubrica = (Rubricator)iterator.next();
		String rubricaThemaCode = rubrica.getThemaCode();
	%>
	  <div class="CollapsiblePanelContent">
	    <ul>
            <% while (themaCode.equals(rubricaThemaCode)) { 
        		rubricaCode = rubrica.getRubricaCode();
            	name = rubrica.getName();
            	countRubrica =  rubrica.getCountRubrica();
            %>
                <li class="стиль8"> 
           		<a href="ViewForSale.jsp?list=rubrica&rubrica=rubrica:<%=rubricaCode%>" target="mainFrame" >
			  			<%=name + " (" + countRubrica + ")"%>
                </a>
                </li>			
			<%
				if(iterator.hasNext()) {
					rubrica = (Rubricator)iterator.next();
					rubricaThemaCode = rubrica.getThemaCode();
					continue;
				} else {
					break;
				}
			}
            
            if(iterator.hasPrevious() && !themaCode.equals(rubricaThemaCode)) {
            	iterator.previous();
            }
			%>
	  
	  	</ul>
	  </div>
	<% } 
	%>
</div>


<% 
}
%>

<p>&nbsp;</p>


<script type="text/javascript">
<!--
var CollapsiblePanel1  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1",  {contentIsOpen:false});
var CollapsiblePanel2  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel2",  {contentIsOpen:false});
var CollapsiblePanel3  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel3",  {contentIsOpen:false});
var CollapsiblePanel4  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel4",  {contentIsOpen:false});
var CollapsiblePanel5  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel5",  {contentIsOpen:false});
var CollapsiblePanel6  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel6",  {contentIsOpen:false});
var CollapsiblePanel7  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel7",  {contentIsOpen:false});
var CollapsiblePanel8  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel8",  {contentIsOpen:false});
var CollapsiblePanel1  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1",  {contentIsOpen:false});
var CollapsiblePanel9  = new Spry.Widget.CollapsiblePanel("CollapsiblePanel9",  {contentIsOpen:false});
var CollapsiblePanel10 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel10", {contentIsOpen:false});
var CollapsiblePanel11 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel11", {contentIsOpen:false});
var CollapsiblePanel12 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel12", {contentIsOpen:false});
var CollapsiblePanel13 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel13", {contentIsOpen:false});
var CollapsiblePanel14 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel14", {contentIsOpen:false});
var CollapsiblePanel15 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel15", {contentIsOpen:false});
var CollapsiblePanel16 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel16", {contentIsOpen:false});
var CollapsiblePanel17 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel17", {contentIsOpen:false});
var CollapsiblePanel18 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel18", {contentIsOpen:false});
var CollapsiblePanel19 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel19", {contentIsOpen:false});
var CollapsiblePanel20 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel20", {contentIsOpen:false});
var CollapsiblePanel21 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel21", {contentIsOpen:false});
var CollapsiblePanel22 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel22", {contentIsOpen:false});
var CollapsiblePanel23 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel23", {contentIsOpen:false});
var CollapsiblePanel24 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel24", {contentIsOpen:false});
var CollapsiblePanel25 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel25", {contentIsOpen:false});
//-->
</script>
</body>
</html>
