<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="Connections/oldbooks.jsp" %>
<%
String id = "0";
if (request.getParameter("id")!=null) id = request.getParameter("id");
String fromPage = "View";
if (request.getParameter("fromPage")!=null) fromPage = request.getParameter("fromPage");
%>

<%
Driver Driverbook = (Driver)Class.forName(MM_oldbooks_DRIVER).newInstance();
Connection Connbook = DriverManager.getConnection(MM_oldbooks_STRING,MM_oldbooks_USERNAME,MM_oldbooks_PASSWORD);
PreparedStatement Statementbook = Connbook.prepareStatement("SELECT * FROM books WHERE ID='" + id + "'");
ResultSet books = Statementbook.executeQuery();
boolean book_isEmpty = !books.next();
boolean book_hasData = !book_isEmpty;
Object books_data;
int book_numRows = 0;
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Заказ книги</title>
<style type="text/css">
<!--
body,td,th {
	color: #000000;
}
body {
	background-color: #B3FDB0;
}
a:link {
	color: #003366;
}
a:visited {
	color: #993300;
}
a:hover {
	color: #CC3333;
}
a:active {
	color: #FF0000;
}
.стиль3 {
	font-size: 18px;
	color: #003300;
}
-->
</style></head>

<body>
<p class="стиль3">Санкт-Петербург. Анатолий Данилов <br/>
				  e-mail:<a href="mailto:info@piteroldbook.ru"><strong>   info@piteroldbook.ru</strong></a>,<a href="mailto:piteroldbook@rambler.ru"><strong>piteroldbook@rambler.ru</strong></a>,<br />
				  тел. +7.921.9529362.<br />
  <strong> С 17 по 26 октября - участник Антикварного салона в ЦДХ, Москва. Приеду - отвечу на все письма.</strong><br />
Цены указаны без стоимости доставки. Оплата - <a href="http://www.alib.ru/nalplat.phtml" target="_blank">наложенный платёж</a>, почтовый перевод, Сбербанк, Внешторгбанк, <a href="http://www.paypal.com/" target="_blank">PayPal</a>.  Не отправляю за границу книги, изданные до 1958 года или на сумму менее  50 USD. Книги в Санкт-Петербурге могу передать у М. Горьковская, 10  мин. пешком, просьба оставить номер телефона для согласования встречи.  Другие мои книги продаются на сайте <strong>WWW.ALIB.RU</strong> под именем  <a href="http://www.alib.ru/bs.php4?bs=piteroldbook"><strong>BS - PiterOldBook</strong></a>,  <a href="http://www.alib.ru/bs.php4?bs=adanilov"><strong>BS-ADanilov</strong></a>.</p>
</body>
</html>
<%
books.close();
Statementbook.close();
Connbook.close();
%>
