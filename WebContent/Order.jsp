<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, entity.*" errorPage="" %>
<%
String id = "0";
if (request.getParameter("id")!=null) id = request.getParameter("id");
String fromPage = "View";
if (request.getParameter("fromPage")!=null) fromPage = request.getParameter("fromPage");

boolean isExists = (Boolean)request.getSession().getAttribute("isExists");
Book book = (Book)request.getSession().getAttribute("book");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Заказ книги</title>
<style type="text/css">
<!--
body,td,th {
	color: #000000;
}
body {
	background-color: #B3FDB0;
}
a:link {
	color: #003366;
}
a:visited {
	color: #993300;
}
a:hover {
	color: #CC3333;
}
a:active {
	color: #FF0000;
}
.стиль4 {
	font-size: 14px;
	font-weight: bold;
}
.стиль5 {font-size: 14px}
-->
</style></head>

<body>

<% if (isExists) { %>
<h1> Сведения о книге номер <%=id%> не найдены!!! </h1>
<% } else { String books_data; %> 

<h3>Заказать книгу:</h3>

<p align="justify" class="стиль14">
         <span class="стиль16">
              <%=(((books_data = book.getAuthor())==null)				?"":books_data)%> &nbsp;
              <%=(((books_data = book.getName())==null)					?"":books_data)%> <em>
              <%=(((books_data = book.getSecondName())==null)			?"":books_data)%>.</em>
              <%=(((books_data = book.getPlace())==null)				?"":books_data)%>. 
              <%=(((books_data = book.getPublisher())==null)			?"":books_data)%>. 
              <%=(((books_data = book.getYear())==null)					?"":books_data)%>.  
              <%=(((books_data = book.getCovers())==null)				?"":books_data)%>. 
              <%=(((books_data = book.getFormat())==null)				?"":books_data)%>.
              <%=(((books_data = book.getDescriptionCovers())==null)	?"":books_data)%> <br/ >
        Цена: <%=(((books_data = "" + book.getPrice())==null)			?"":books_data)%>              
        </span></p>
<p align="justify" class="стиль4">Перед подтверждением заказа проверьте правильность своего e-mail.
<br/>
Введенные данные не сохраняются на сервере и поэтому не могут быть никем использованы.</p>
<table width="655">
  <tbody>
    <tr>
      <td width="647">
      <form action="Request.jsp?id=<%=id%>" method="post">
        <p> Фамилия, Имя Отчество (полностью) * 
          <input size="80" name="FIO" value="" type="text" />
  <br />

e-mail: * 
<input size="20" name="E_MAIL" value="" type="text" />
 
Мой телефон:
<input size="15" name="TEL" value="" type="text" />
        </p>
        <p class="стиль5">Необязательные поля:</p>
        <p>Страна
          <input size="20" name="STATE" value="" type="text" />
          Индекс 
          <input size="6" name="INDEX" value="" type="text" />          
          <br />
          Область, Город 
          <input size="62" name="TOWN" value="" type="text" />
          <br />
          Улица 
          <input size="35" name="STREET" value="" type="text" />
          
          дом 
          <input size="3" name="HOUSE" value="" type="text" />
          
          корпус 
          <input size="3" name="BUILDING" value="" type="text" />
          
          кв. 
          <input size="3" name="FLAT" value="" type="text" />
            <br />
          <input name="id" value="<%=id%>" type="hidden" />
          <input name="fromPage" value="<%=fromPage%>" type="hidden" />
			<br />
          <input name="orderButton" value="Заказать" type="submit" />
        </p>
      </form>
      </td>
    </tr>
  </tbody>
</table>


<% } %>
</body>
</html>
