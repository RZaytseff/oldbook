<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="Connections/oldbooks.jsp" %>
<%
String id = "0";
if (request.getParameter("id")!=null) id = request.getParameter("id");
%>
<%
Driver Driverbook = (Driver)Class.forName(MM_oldbooks_DRIVER).newInstance();
Connection Connbook = DriverManager.getConnection(MM_oldbooks_STRING,MM_oldbooks_USERNAME,MM_oldbooks_PASSWORD);
PreparedStatement Statementbook = Connbook.prepareStatement("SELECT * FROM  books WHERE ID='" + id + "'");
ResultSet books = Statementbook.executeQuery();
boolean book_isEmpty = !books.next();
boolean book_hasData = !book_isEmpty;
Object books_data;
int book_numRows = 0;
%>
<% String icon, largeImage1, largeImage2, largeImage3;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Подробнее о книге</title>
<style type="text/css">
<!--
body,td,th {
	color: #000000;
}
body {
	background-color: #B3FDB0;
}
a:link {
	color: #003366;
}
a:visited {
	color: #993300;
}
a:hover {
	color: #CC3333;
}
a:active {
	color: #FF0000;
}
-->
</style></head>

<body>

<% if (book_isEmpty) { %>
<h1> Сведения о книге номер <%=id%> не найдены!!! </h1>
<% } else { %> 
<p align="justify" class="стиль14">
         <span class="стиль16">
			  <%=(((books_data = books.getObject("AUTHOR"))==null || books.wasNull())?"":books_data)%>
              <br/>
			  <%=(((books_data = books.getObject("NAME"))==null || books.wasNull())?"":books_data)%>
              <em><%=(((books_data = books.getObject("SECOND_NAME"))==null || books.wasNull())?"":books_data)%></em>
              <br/>
			  <%=(((books_data = books.getObject("PLACE"))==null || books.wasNull())?"":books_data)%> 
			  <%=(((books_data = books.getObject("PUBLISHER"))==null || books.wasNull())?"":books_data)%>
              <%=(((books_data = books.getObject("YEAR"))==null || books.wasNull())?"":books_data)%>.  
              <%=(((books_data = books.getObject("PAGES"))==null || books.wasNull())?"":books_data)%>. 
              <br/> 
			  <%=(((books_data = books.getObject("COVERS"))==null || books.wasNull())?"":books_data)%>. 
			  <%=(((books_data = books.getObject("FORMAT"))==null || books.wasNull())?"":books_data)%>.
              <%=(((books_data = books.getObject("DESCRIPTION"))==null || books.wasNull())?"":books_data)%>
              <br/>
              Состояние: 
              <%=(((books_data = books.getObject("STATE_CONDITION"))==null || books.wasNull())?"":books_data)%>.  
			  <%=(((books_data = books.getObject("DESCRIPTION_COVERS"))==null || books.wasNull())?"":books_data)%> 
              <br/ >              
          Цена: 
		      <%=(((books_data = books.getObject("PRICE"))==null || books.wasNull())?"":books_data)%>              
          </span>
</p>

<% 
icon = (((books_data = books.getObject("SMALL_IMAGE"))         ==null || books.wasNull())?"":books_data).toString().trim();
largeImage1 = (((books_data = books.getObject("LARGE_IMAGE_1"))==null || books.wasNull())?"":books_data).toString().trim();
largeImage2 = (((books_data = books.getObject("LARGE_IMAGE_2"))==null || books.wasNull())?"":books_data).toString().trim();
largeImage3 = (((books_data = books.getObject("LARGE_IMAGE_3"))==null || books.wasNull())?"":books_data).toString().trim();
%>

		<% if (icon.length()>0) { %>
                    <img src="images/<%=icon%>" alt="Обложка: Для измениения размера изображения достаточно щёлнуть на нём мышкой" 
                    		id="image<%=id%>icon" width="160" 
                                 onclick= "var icon = document.getElementById('image<%=id%>icon');  
                    				if (icon.width==160) {icon.width=760;} else {icon.width=160;}"/>
        <% } %>
		&nbsp;&nbsp;
		<% if (largeImage1.length()>0) { %>
        <img src="images/<%=largeImage1%>" alt="Титульный лист: Для измениения размера изображения достаточно щёлнуть на нём мышкой" 
        		id="image<%=id%>large1" width="160" 
                    onclick= "var icon = document.getElementById('image<%=id%>large1');  
                    		if (icon.width==160) {icon.width=760;} else {icon.width=160;}"/>
        <% } %>
		&nbsp;&nbsp;
		<% if (largeImage2.length()>0) { %>
        <img src="images/<%=largeImage2%>" alt="Для измениения размера изображения достаточно щёлнуть на нём мышкой" 
        		id="image<%=id%>large2" width="160" 
                    onclick= "var icon = document.getElementById('image<%=id%>large2'); 
                    		if (icon.width==160) {icon.width=760;} else {icon.width=160;}"/>
        <% } %>
		&nbsp;&nbsp;
		<% if (largeImage3.length()>0) { %>
        <img src="images/<%=largeImage3%>" alt="Для измениения размера изображения достаточно щёлнуть на нём мышкой" 
        		id="image<%=id%>large3" width="160" 
                    onclick= "var icon = document.getElementById('image<%=id%>large3'); 
                    		if (icon.width==160) {icon.width=760;} else {icon.width=160;}"/>
        <% } %>

<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <a href="Order?id=<%=id%>&fromPage=Detail" target="mainFrame">Оформить заказ </a> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="javascript:history.go(-1);" target="mainFrame">Вернуться к списку книг</a>	
          
<table border="0" cellpadding="3" cellspacing="3" width="49%">
  <tbody>
    <tr>
      <td width="98%"><hr />
          <p><strong>Здесь можно спросить о книге:</strong></p>
        <form action="Question.jsp?id=<%=id%>" method="post">
            <textarea name="RequestForDetail" cols="90" rows="12" id="RequestForDetail">
Здравствуйте, Анатолий. 
    
У меня к Вам вопрос о книге:         
  <%=(((books_data = books.getObject("AUTHOR"))==null || books.wasNull())?"":books_data)%>
  <%=(((books_data = books.getObject("NAME"))==null || books.wasNull())?"":books_data)%> <%=(((books_data = books.getObject("SECOND_NAME"))==null || books.wasNull())?"":books_data)%>
  <%=(((books_data = books.getObject("PLACE"))==null || books.wasNull())?"":books_data)%> <%=(((books_data = books.getObject("PUBLISHER"))==null || books.wasNull())?"":books_data)%>. <%=(((books_data = books.getObject("YEAR"))==null || books.wasNull())?"":books_data)%>. 
              
              
Мой электронный адрес:     
 тел.
 			</textarea>
       	  	<br />
            <input name="request" value="Отправить" type="submit" />
          </form>
      </td>

    </tr>
  </tbody>
</table>
<% } %>
</body>
</html>
<%
books.close();
Statementbook.close();
Connbook.close();
%>
