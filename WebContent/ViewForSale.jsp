﻿<%@ page contentType="text/html; charset=utf-8" %>
<%@page import="java.sql.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="entity.*"%>
<%@page import="model.*"%>

<%
BookFilter filter = new BookFilter();
String thema = "";
String rubrica = "";
String list = request.getParameter("list");
String action = request.getParameter("action");

boolean isListThema = false;
boolean isListRubrica = false;
boolean isListFilter = false;

boolean isAllThema = false;
boolean isAllRubrica = false;

if (request.getParameter("list")!=null && request.getParameter("list").length() > 0) {
	list = request.getParameter("list").trim();
	if (list.regionMatches(true,0,"thema",0,5)) {
		isListThema = true;
	} else if (list.regionMatches(true,0,"rubrica",0,7) ) {
		isListRubrica = true;
	} else  if (list.regionMatches(true,0,"filter",0,6)) {
		isListFilter = true;
	} 
} else {
	isListFilter = true;
}

if (request.getParameter("last")!=null && request.getParameter("last").length() > 0) {
	String last = request.getParameter("last").trim();
	if (last.equals("all")) 
		{     }
	else {                  
		filter.setPeriod(last);
	}
	if (last.equals("0")) {
		out.println("<h1 class='стиль1'> Поступления за сегодня </h1>");

	} else if (last.equals("1")) {
		out.print("<h1 class='стиль1'> Поступления за последние 2 дня  </h1>");
	} else if (last.equals("2")) {
		out.print("<h1 class='стиль1'> Поступления за последние 3 дня  </h1>");
	} else if (last.equals("6")) {
		out.print("<h1 class='стиль1'> Поступления за последнюю неделю </h1>");
	} else if (last.equals("29") || last.equals("30")) {
		out.print("<h1 class='стиль1'> Поступления за последний месяц </h1>");
	}
}

// Отладочный вывод названия кодировки для проверки
//		out.println( "Encoding: " +    response.getCharacterEncoding() + "\n");

//	String requestEnc = "CP1251"; //особенность Resin
	String requestEnc = "ISO-8859-1"; //особенность Tomcat eatj.com
//	String clientEnc  = "ISO-8859-1";
//	String requestEnc = "UTF-8"; 
	String clientEnc  = "UTF-8";

if (request.getParameter("author")!=null && request.getParameter("author").length() > 0) {
	String author = "";
	author = request.getParameter("author");
	author = new String(author.getBytes(requestEnc),clientEnc);
	filter.setAuthor(author);
}

if (request.getParameter("name")!=null && request.getParameter("name").length() > 0) {
	String name = "";
	name = request.getParameter("name");
	name = new String(name.getBytes(requestEnc),clientEnc);
	filter.setName(name);
}

if (request.getParameter("context")!=null && request.getParameter("context").length() > 0) {
	String context = "";
	context = request.getParameter("context");
	context = new String(context.getBytes(requestEnc),clientEnc);
	filter.setContext(context);
}

if (request.getParameter("from_year")!=null && request.getParameter("from_year").length() > 0) {
	String from_year = request.getParameter("from_year");
	filter.setFromYear(from_year);
}

if (request.getParameter("to_year")!=null && request.getParameter("to_year").length() > 0) {
	String to_year = request.getParameter("to_year");
	filter.setTillYear(to_year);
}

if (request.getParameter("from_price")!=null && request.getParameter("from_price").length() > 0) {
	String from_price = request.getParameter("from_price");
	try {
		filter.setFromPrice(Integer.parseInt(from_price));		
	} catch (Exception e) {}
}

if (request.getParameter("to_price")!=null && request.getParameter("to_price").length() > 0) {
	String to_price = request.getParameter("to_price");
	try {
		filter.setTillPrice(Integer.parseInt(to_price));	
	} catch (Exception e) {}
	
}


if (request.getParameter("rubrica")!=null && request.getParameter("rubrica").length() > 0) {
	String themaRubrica = request.getParameter("rubrica").trim();
	if (themaRubrica.regionMatches(true,0,"thema:",0,6)) {
		thema = themaRubrica.substring(6).trim();
		if (thema.regionMatches(true,0,"all",0,3)) {
			thema = "";
		} else {
			filter.setThemaCode(thema);
		}
	} else if (themaRubrica.regionMatches(true,0,"rubrica:",0,8) ) {
		rubrica = themaRubrica.substring(8).trim();
		if (themaRubrica.regionMatches(true,0,"all",0,3)) {
			rubrica = "";
		} else {
			filter.setRubricaCode(rubrica);
		}
	}
}

String sort = "data DESC";
if (request.getParameter("sort")!=null && request.getParameter("sort").trim().length() > 0)
	sort = request.getParameter("sort").trim();
filter.setSort(sort);


System.err.println(" filter = " + filter);
//out.println("SELECT * FROM books  " + filter);
%>

<jsp:useBean id="rubricatorBean" class="bean.RubricatorBean" scope="session"/>
<% List<Book> books = null;
if(isListThema)
	books = rubricatorBean.getThemaBookList(thema); 
if(isListRubrica)
	books = rubricatorBean.getRubricaBookList(rubrica); 
if(isListFilter)
	books = rubricatorBean.getSearchBookList(filter); 

boolean books_isEmpty = books == null || books.size() == 0;
boolean books_hasData = !books_isEmpty;

Object books_data;
int books_numRows = 0;

String pageSize = "5";

if (request.getParameter("PageSize") != null && request.getParameter("PageSize").length() != 0 ) 
						pageSize = request.getParameter("PageSize");

int Repeat1__numRows = Integer.parseInt(pageSize); // was   -1 
int Repeat1__index = 0;
books_numRows += Repeat1__numRows;

// *** Recordset Stats, Move To Record, and Go To Record: declare stats variables
int books_first = 1;
int books_last  = 1;
int books_total = -1;

if (books_isEmpty) {
  books_total = books_first = books_last = 0;
}

//set the number of rows displayed on this page
if (books_numRows == 0) {
  books_numRows = 1;
}


// *** Recordset Stats: if we don't know the record count, manually count them
if (books_total == -1) {

  // count the total records by iterating through the recordset
    books_total = books.size();

  // set the number of rows displayed on this page
  if (books_numRows < 0 || books_numRows > books_total) {
    books_numRows = books_total;
  }
//	out.println("books_total = " + books_total);
  // set the first and last displayed record
  books_first = Math.min(books_first, books_total);
  books_last  = Math.min(books_first + books_numRows - 1, books_total);
// out.println("books_first = " + books_first);
// out.println("books_last = " + books_last); 
}

String MM_paramName = "";

// *** Move To Record and Go To Record: declare variables

Iterator MM_rs = books.iterator();
int       MM_rsCount = books_total;
int       MM_size = books_numRows;
String    MM_uniqueCol = "";
          MM_paramName = "";
int       MM_offset = 0;
boolean   MM_atTotal = false;
boolean   MM_paramIsDefined = (MM_paramName.length() != 0 && request.getParameter(MM_paramName) != null);


// *** Move To Record: handle 'index' or 'offset' parameter

if (!MM_paramIsDefined && MM_rsCount != 0) {

  //use index parameter if defined, otherwise use offset parameter
  String r = request.getParameter("index");
  if (r==null) r = request.getParameter("offset");
  if (r!=null) MM_offset = Integer.parseInt(r);

  // if we have a record count, check if we are past the end of the recordset
  if (MM_rsCount != -1) {
    if (MM_offset >= MM_rsCount || MM_offset == -1) {  // past end or move last
      if (MM_rsCount % MM_size != 0)    // last page not a full repeat region
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  //move the cursor to the selected record
  int i;
  for (i=0; books_hasData && (i < MM_offset || MM_offset == -1); i++) {
	MM_rs.next();  
	books_hasData = MM_rs.hasNext();
  }
  if (!books_hasData) MM_offset = i;  // set MM_offset to the last possible record
}


// *** Move To Record: if we dont know the record count, check the display range

if (MM_rsCount == -1) {

  // walk to the end of the display range for this page
  int i = 0;
  for (i=MM_offset; books_hasData && (MM_size < 0 || i < MM_offset + MM_size); i++) {
    books_hasData = books.iterator().hasNext();
  }

 
  // if we walked off the end of the recordset, set MM_rsCount and MM_size
  if (!books_hasData) {
    MM_rsCount = i;
    if (MM_size < 0 || MM_size > MM_rsCount) MM_size = MM_rsCount;
  }

  // if we walked off the end, set the offset based on page size
  if (!books_hasData && !MM_paramIsDefined) {
    if (MM_offset > MM_rsCount - MM_size || MM_offset == -1) { //check if past end or last
      if (MM_rsCount % MM_size != 0)  //last page has less records than MM_size
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  // move the cursor to the selected record
  for (i=0; books_hasData && i < MM_offset; i++) {
	books.iterator().next();
    books_hasData = books.iterator().hasNext();
  }
}


// *** Move To Record: update recordset stats

// set the first and last displayed record
books_first = MM_offset + 1;
books_last  = MM_offset + MM_size;
if (MM_rsCount != -1) {
  books_first = Math.min(books_first, MM_rsCount);
  books_last  = Math.min(books_last, MM_rsCount);
}

// set the boolean used by hide region to check if we are on the last record
MM_atTotal  = (MM_rsCount != -1 && MM_offset + MM_size >= MM_rsCount);


// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

String MM_keepBoth,MM_keepURL="",MM_keepForm="",MM_keepNone="";
String[] MM_removeList = { "index", MM_paramName };

// create the MM_keepURL string
if (request.getQueryString() != null) {
  MM_keepURL = '&' + request.getQueryString();
  for (int i=0; i < MM_removeList.length && MM_removeList[i].length() != 0; i++) {
  int start = MM_keepURL.indexOf(MM_removeList[i]) - 1;
    if (start >= 0 && MM_keepURL.charAt(start) == '&' &&
        MM_keepURL.charAt(start + MM_removeList[i].length() + 1) == '=') {
      int stop = MM_keepURL.indexOf('&', start + 1);
      if (stop == -1) stop = MM_keepURL.length();
      MM_keepURL = MM_keepURL.substring(0,start) + MM_keepURL.substring(stop);
    }
  }
}

// add the Form variables to the MM_keepForm string
if (request.getParameterNames().hasMoreElements()) {
  java.util.Enumeration items = request.getParameterNames();
  while (items.hasMoreElements()) {
    String nextItem = (String)items.nextElement();
    boolean found = false;
    for (int i=0; !found && i < MM_removeList.length; i++) {
      if (MM_removeList[i].equals(nextItem)) found = true;
    }
    if (!found && MM_keepURL.indexOf('&' + nextItem + '=') == -1) {
      MM_keepForm = MM_keepForm + '&' + nextItem + '=' + java.net.URLEncoder.encode(request.getParameter(nextItem));
    }
  }
}

String tempStr = "";
for (int i=0; i < MM_keepURL.length(); i++) {
  if (MM_keepURL.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepURL.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepURL.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepURL.charAt(i);
}
MM_keepURL = tempStr;

tempStr = "";
for (int i=0; i < MM_keepForm.length(); i++) {
  if (MM_keepForm.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepForm.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepForm.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepForm.charAt(i);
}
MM_keepForm = tempStr;

// create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL + MM_keepForm;
if (MM_keepBoth.length() > 0) MM_keepBoth = MM_keepBoth.substring(1);
if (MM_keepURL.length() > 0)  MM_keepURL = MM_keepURL.substring(1);
if (MM_keepForm.length() > 0) MM_keepForm = MM_keepForm.substring(1);


// *** Move To Record: set the strings for the first, last, next, and previous links

String MM_moveFirst,MM_moveLast,MM_moveCurrent,MM_moveNext,MM_movePrev;
{
  String MM_keep = MM_keepBoth;
  String MM_keepMove = MM_keepBoth;  // keep both Form and URL parameters for moves
  String MM_moveParam = "index=";

  // if the page has a repeated region, remove 'offset' from the maintained parameters
  if (MM_size > 1) {
    MM_moveParam = "offset=";
    int start = MM_keepMove.indexOf(MM_moveParam);
    if (start != -1 && (start == 0 || MM_keepMove.charAt(start-1) == '&')) {
      int stop = MM_keepMove.indexOf('&', start);
      if (start == 0 && stop != -1) stop++;
      if (stop == -1) stop = MM_keepMove.length();
      if (start > 0) start--;
      MM_keepMove = MM_keepMove.substring(0,start) + MM_keepMove.substring(stop);
    }
  }

  // set the strings for the move to links
  StringBuffer urlStr = new StringBuffer(request.getRequestURI()).append('?').append(MM_keepMove);
  if (MM_keepMove.length() > 0) urlStr.append('&');
  urlStr.append(MM_moveParam);
  MM_moveFirst = urlStr + "0";
  MM_moveLast  = urlStr + "-1";
  MM_moveCurrent = urlStr + Integer.toString(MM_offset);
    if (MM_moveCurrent != null && MM_moveCurrent.length() != 0) {
    MM_moveParam = "PageSize=";
    int start = MM_moveCurrent.indexOf(MM_moveParam);
	if (start != -1 && (start == 0 || MM_moveCurrent.charAt(start-1) == '&' || MM_moveCurrent.charAt(start-1) == '?')) {
      int stop = MM_moveCurrent.indexOf('&', start);
      if (start == 0 && stop != -1) stop++;
      if (stop == -1) stop = MM_moveCurrent.length();
      if (start > 0) start--;
      MM_moveCurrent = MM_moveCurrent.substring(0,start+1) + MM_moveCurrent.substring(stop+1);
    }
  }

  MM_moveNext  = urlStr + Integer.toString(MM_offset+MM_size);
  MM_movePrev  = urlStr + Integer.toString(Math.max(MM_offset-MM_size,0));

}
//  out.println("urlStr = " + urlStr);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Редкие книги</title>
<style type="text/css">
<!--
body {
	background-color: #B3FDB0;
}
.стиль1 {
	font-size: 18px;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #990000;
}	
.стиль14 {
	font-weight: bold;
	color: #336600;
	font-size: 12px;
}
.стиль15 {font-size: 16px}
.стиль16 {color: #003300}
.стиль17 {color: #009900}
-->
</style>
</head>

<body>
              
<table width="98%"  border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="68%"><h1 class='стиль1'>
	    <%              
			if (isAllThema) { 
				out.println(" Все темы ");
			} else	if (isAllRubrica) { 
				out.println("<h1 class='стиль1'> Все рубрики ");
			} else if (isListRubrica) {
					out.println(rubricatorBean.getRubricaName(rubrica)); 
			} else if (isListThema) {
					out.println(rubricatorBean.getThemaName(thema)); 
			}
		%>
		</h1>
	</td>

<%  
	if (3 <= books_total ) { %>
	  <form name="PageSizeForm" id="PageSizeForm" method="post" action="<%=MM_moveCurrent%>">
	    <td width="26%"><div align="right">
	      <input type="submit" value="Установить количество книг на странице" />
	      </div></td>
        <td width="6%">
          <div align="left">
            <select name="PageSize">
                <option value="3">3</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="-1">все</option>
            </select>
          </div></td>
  </form>
<% } %>
  </tr>
</table>


             
<% if (books_isEmpty) { %>  
            <h1 align="center" class="стиль15">   По данному запросу книг не найдено! </h1>
              <% } else {%>
              
              

<table border="0" width="100%">

            <% 
              ListIterator<Book> iterator = books.listIterator();
              Book book = iterator.next();
              while ((books_hasData)&&(Repeat1__numRows-- != 0)) { 
				 int COD = book.getId();
			%>
                    
<tr>
<td width="100%">
<br/>
              <span class="стиль16">
              <%=book.getAuthor()%> &nbsp;
              <%=book.getName()%> 
              <em>
              <%=(book.getSecondName() != null ? book.getSecondName() : "")%>
              </em>
              <%=book.getPlace()%>. 
              <%=book.getYear()%>. 
              <%=book.getPages()%>. 
              <%=book.getPublisher()%>. 
              <%=book.getFormat()%>. 
              <%=book.getCovers()%>. <br/>
              <%=(book.getDescription() != null ? book.getDescription() : "")%>
	          Состояние: <%=book.getCondition()%>. 
	          <%=(book.getComment() != null ? book.getComment() : "")%> <br/ >
	          Цена: <%=book.getPrice()%> 
	          
             </span>
          
            <br/>
  		<div align="center">

              <a href="Order?id=<%=COD%>&fromPage=View" target="mainFrame">Оформить заказ </a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="Detail.jsp?id=<%=COD%>" target="mainFrame">Подробнее</a>		
        
        </div></td></tr>               
        <%
  Repeat1__index++;
  if(iterator.hasNext())
  	book = iterator.next();      
  books_hasData = Repeat1__index < books.size();
}
%>
</table>

<% if (Integer.parseInt(pageSize) <= books_total ) { %>
<br/>
<br/>
<table border="0" width="64%" align="center">
  <tr>
    <td width="5%" height="28" align="center">
      <% if (MM_offset !=0) { %>
      <a href="<%=MM_moveFirst%>"><img src="images/First.gif" border=0></a>
    <% } /* end MM_offset != 0 */ %>    </td>
    <td width="4%" align="center">
      <% if (MM_offset !=0) { %>
      <a href="<%=MM_movePrev%>"><img src="images/Previous.gif" border=0></a>
      <% } /* end MM_offset != 0 */ %>
    </td>
    <td width="76%" align="center">
	<table width="92%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td width="100%">Книги с&nbsp; <%=(books_first)%>&nbsp; по &nbsp; <%=(books_last)%> 
		&nbsp;Всего:&nbsp; <%=(books_total)%></td>
      </tr>

    </table>
	</td>
    <td width="11%" align="center">
      <% if (!MM_atTotal) { %>
      <a href="<%=MM_moveNext%>"><img src="images/Next.gif" border=0></a>
      <% } /* end !MM_atTotal */ %>
    </td>
    <td width="4%" align="center">
      <% if (!MM_atTotal) { %>
      <a href="<%=MM_moveLast%>"><img src="images/Last.gif" border=0 alt="Последняя страница"></a>
      <% } /* end !MM_atTotal */ %>
    </td>
  </tr>
</table>
<% } 
} %>

<p>&nbsp;</p>
</body>
</html>

