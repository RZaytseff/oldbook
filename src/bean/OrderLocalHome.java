package bean;

import javax.ejb.*;

public interface OrderLocalHome extends EJBLocalHome
{
    Order create() throws javax.ejb.CreateException;
}
