package bean;

import java.rmi.RemoteException;

import javax.ejb.*;
import dao.*;
import entity.*;

@Stateless
//@LocalBean
//@Local(OrderLocal.class)
//@Remote(OrderRemote.class)
//@RemoteHome(OrderRemoteHome.class)
//@LocalHome(OrderLocalHome.class)
@EJB(beanInterface = Order.class, beanName = "OrderBean", name = "OrderBean")
public class OrderBean implements Order, javax.ejb.SessionBean {
	private static final long serialVersionUID = 1L;

	@EJB
	protected BookDAO bookDAO;
	
	@EJB
	protected BuyerDAO buyerDAO;
	
    @Init
    public void create() {
    }
    
	@Override
	public int order(int bookId, Buyer buyer) {
		return 0;
	}

	@Override
	public Book getBook(int id) {
		return bookDAO.findById(id);
	}

	@Override
	public void ejbActivate() throws EJBException, RemoteException {
		System.out.println("Order Remote ejbActivate");
	}

	@Override
	public void ejbPassivate() throws EJBException, RemoteException {
		System.out.println("Order Remote ejbPassivate");
	}

	@Override
	public void ejbRemove() throws EJBException, RemoteException {
		System.out.println("Order Remote ejbRemove");
	}

	@Override
	public void setSessionContext(SessionContext arg0) throws EJBException, RemoteException {
		System.out.println("Order Remote setSessionContext");
	}

//	@Override
	public EJBLocalHome getEJBLocalHome() throws EJBException {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
	public Object getPrimaryKey() throws EJBException {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
	public boolean isIdentical(EJBLocalObject arg0) throws EJBException {
		// TODO Auto-generated method stub
		return false;
	}

//    @Remove
//	@Override
	public void remove() throws RemoveException, EJBException {
		System.out.println("Order remove");
	}

}
