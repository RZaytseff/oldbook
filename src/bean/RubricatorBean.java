package bean;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import dao.*;
import entity.*;
import model.BookFilter;
import model.Rubricator;

@Stateless
@Local(RubricatorLocal.class)
public class RubricatorBean implements RubricatorLocal {

	@EJB
	protected RubricatorDAO rubricatorDAO;

	@EJB
	protected BookDAO bookDAO;
	
	@Override
	@SuppressWarnings({ })
	public List<Rubricator> getRubricator() {
		List<Rubrica> rubricator = getRubricatorDAO().list();
		List<Rubricator> extendedRubricator = new LinkedList<Rubricator>();
		if(rubricator != null && rubricator.size() > 0) {
			for(Rubrica rubrica: rubricator) {
				Rubricator extendRubrica = new Rubricator();
				extendRubrica.setThemaCode(rubrica.getThemaCode());
				extendRubrica.setRubricaCode(rubrica.getRubricaCode());
				extendRubrica.setName(rubrica.getName());
				extendRubrica.setIsThema(rubrica.isThema());
				extendRubrica.setHasRubrices(getRubricatorDAO().count("themaCode", rubrica.getThemaCode()) > 1);
				extendRubrica.setCountThema  (getBookDAO().count("codeThema", rubrica.getThemaCode()));
				extendRubrica.setCountRubrica(bookDAO.count("codeRubrica", rubrica.getRubricaCode()));
				extendedRubricator.add(extendRubrica);
			}
		} 
		return extendedRubricator;
	}

	@Override
	public List<Book> getThemaBookList(String themaCode) {
		return getBookDAO().list("codeThema", themaCode);
	}

	@Override
	public List<Book> getRubricaBookList(String rubricaCode) {
		return getBookDAO().list("codeRubrica", rubricaCode);
	}
	
	@Override
	public List<Book> getNewBookList(int days) {
		return getBookDAO().list(null, null, " DATEDIFF(NOW(), data) < " + days);
	}

	@Override
	public List<Book> getSearchBookList(BookFilter filter) {
		return getBookDAO().list(filter);
	}
	
	@Override
	public String getThemaName(String themaCode) {
		return getRubricatorDAO().read("themaCode", themaCode).getName();
	}

	@Override
	public String getRubricaName(String rubricaCode) {
		return getRubricatorDAO().read("rubricaCode", rubricaCode).getName();
	}

	protected RubricatorDAO getRubricatorDAO() {
		if(rubricatorDAO == null)
			try {
				rubricatorDAO = (RubricatorDAO)new InitialContext().lookup("java:global/OldBook/RubricatorDAO!dao.RubricatorDAO");
			} catch (NamingException e) {
				rubricatorDAO = new RubricatorDAO();
			}
		return rubricatorDAO;
	}

	protected BookDAO getBookDAO() {
		if(bookDAO == null)
			try {
				bookDAO = (BookDAO)new InitialContext().lookup("java:global/OldBook/BookDAO!dao.BookDAO");
			} catch (NamingException e) {
				bookDAO = new BookDAO();
			}
		return bookDAO;
	}

}
