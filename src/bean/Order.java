package bean;

import entity.Book;
import entity.Buyer;

public interface Order {
	int order(int bookId, Buyer buyer);
	Book getBook(int bookId);
}
