package bean;

import java.util.List;

import javax.ejb.Local;

import entity.Book;
import model.BookFilter;
import model.Rubricator;

@Local
public interface RubricatorLocal {
	List<Rubricator> getRubricator();
	List<Book> getThemaBookList(String themaCode);
	List<Book> getRubricaBookList(String themaCode);
	List<Book> getNewBookList(int days);
	String getRubricaName(String rubricaCode);
	String getThemaName(String themaCode);
	List<Book> getSearchBookList(BookFilter filter);
}
