package bean;

import javax.ejb.RemoteHome;

@RemoteHome(value = Order.class)
public interface OrderRemoteHome extends javax.ejb.EJBHome
{
    Order create() throws java.rmi.RemoteException, javax.ejb.CreateException;
}
	