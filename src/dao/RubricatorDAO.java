package dao;

import javax.ejb.Singleton;
import entity.Rubrica;


/**
 * @author Roman Zaytseff
 * @e-mail RZaytseff@mail.ru
 */

/** DAO for Rubrica's CRUD + List + Count operations */
@Singleton
public class RubricatorDAO extends _DAO<Rubrica> {
	
	@Override
	public boolean isUnique(Rubrica rubrica) {
		return count("COD_RUBRICA", rubrica.getRubricaCode()) == 0
				&& count("RUBRICA", rubrica.getName()) == 0;
	}

}
