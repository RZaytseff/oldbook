package dao;

import java.util.List;

/**
 * @author Roman Zaytseff
 * @e-mail RZaytseff@mail.ru
 */

import javax.ejb.Singleton;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import entity.Book;
import model.BookFilter;

/** DAO for book's info */
@Singleton
public class BookDAO extends _DAO<Book> {
	
	String tableName = tableName() + ".";
	
	public int countThema(String thema) {
		return count("codeThema", thema);
	}
	
	public int countRubrica(String rubrica) {
		return count("codeRubrica", rubrica);
	}
	
	private int rubricatorCount(String filter) {
		Query query;
		String qlString = "SELECT count(book) FROM " + entityName() + " as book WHERE " + filter;		
		try {
			query = em.createNativeQuery(qlString);	
			return ((Long) query.getSingleResult()).intValue(); 
			
		} catch (NoResultException e) {
			logger.error(e);
			return 0;
		}
		
	}
	
	@Override
	public boolean isUnique(Book book) {
		return count("book.name + book.secondName", book.getName() + book.getSecondName()) == 0;
	}

	public List<Book> list(BookFilter bookFilter) {
		String filter = ""; // "(" + tableName + "status is null OR " + tableName + "status = 0 OR " + tableName + "status = 1) ";
		if(bookFilter.getAuthor() != null) 
			filter += tableName + "author like '%" + bookFilter.getAuthor().trim() + "%' ";
		if(bookFilter.getName() != null) 
			filter += " AND " + tableName + "name like '%" + bookFilter.getName().trim() + "%' ";
		if(bookFilter.getContext() != null) {
			String context = bookFilter.getContext().trim();
			filter += " AND (";
			filter += tableName + "name like '%" + context + "%' ";
			filter += " OR " + tableName + "secondName like '%" + context + "%' ";
			filter += " OR " + tableName + "secondName like '%" + context + "%' ";
			filter += " OR " + tableName + "comment like '%" + context + "%' ";
			filter += " OR " + tableName + "description like '%" + context + "%' ";
			filter += " OR " + tableName + "descriptionCovers like '%" + context + "%' ";
			filter += ")";
		}
		if(bookFilter.getFromYear() != null) 
			filter += " AND " + tableName + "year >= '" + bookFilter.getFromYear().trim() + "'";
		if(bookFilter.getTillYear() != null) 
			filter += " AND " + tableName + "year <= '" + bookFilter.getTillYear().trim() + "'";

		if(bookFilter.getTillPrice() > 0) 
			filter += " AND " + tableName + "price <= " + bookFilter.getTillPrice();
		if(bookFilter.getFromPrice() > 0) 
			filter += " AND " + tableName + "price >= " + bookFilter.getFromPrice();

		if(bookFilter.getThemaCode() != null) 
			filter += " AND " + tableName + "codeThema = '" + bookFilter.getThemaCode() + "'";
		if(bookFilter.getRubricaCode() != null) 
			filter += " AND " + tableName + "codeRubrica = '" + bookFilter.getRubricaCode() + "'";

		if(bookFilter.getSort() != null) 
			filter += " ORDER BY " + tableName + bookFilter.getSort().toLowerCase();
		logger.error(filter);
		return list(null, null, filter);
	}

}
