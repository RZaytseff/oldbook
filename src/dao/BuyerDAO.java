package dao;

/**
 * @author Roman Zaytseff
 * @e-mail RZaytseff@mail.ru
 */

import util.Logger;

import javax.ejb.Singleton;
import entity.Buyer;

/** DAO for buyer's info */
@Singleton
public class BuyerDAO extends _DAO<Buyer>{
	
	protected Logger logger = new Logger(BuyerDAO.class);
		
	@Override
	public boolean isUnique(Buyer buyer) {
		return count("mail", buyer.getMail()) == 0;
	}

}
