package entity;

import java.util.*;
import javax.persistence.*;

@Entity
@Table(name="rubrices") 
public class Rubrica extends _Entity implements java.io.Serializable {
	
	private static final long serialVersionUID = -1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;
	
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "book_rubrica",
       joinColumns = {@JoinColumn(name = "thema_id")},
       inverseJoinColumns = {@JoinColumn(name = "book_id")})
    protected List<Book> bookThemaList; 
    
    @Column(name = "COD_THEMA")
	protected String themaCode; 
	
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "book_rubrica",
       joinColumns = {@JoinColumn(name = "rubrica_id")},
       inverseJoinColumns = {@JoinColumn(name = "book_id")})
	protected List<Book> bookRubricaList; 
  
    @Column(name = "COD_RUBRICA")
    protected String rubricaCode;

	@Column(name = "RUBRICA")
	protected String name;
  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getThemaCode() {
		return themaCode;
	}
	public void setThemaCode(String themaCode) {
		this.themaCode = themaCode;
	}
	public String getRubricaCode() {
		return rubricaCode;
	}
	public void setRubricaCode(String rubricaCode) {
		this.rubricaCode = rubricaCode;
	}

	public List<Book> getBookThemaList() {
		return bookThemaList;
	}
	
	public List<Book> getBookRubricaList() {
		return bookRubricaList;
	}
	public boolean isThema() {
		if(themaCode == null || rubricaCode == null)
			return false;
		
		return rubricaCode.equals(themaCode);
	}
	
} 