package entity;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name="books") 
public class Book  extends _Entity implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "NAME")
	private String name; 
	
	@Column(name = "SECOND_NAME")
	private String secondName;

   @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
   @JoinTable(name = "book_rubrica",
      joinColumns = {@JoinColumn(name = "book_id")},
      inverseJoinColumns = {@JoinColumn(name = "thema_id")})
	private List<Rubrica> themaSet;

 	@Column(name = "COD_THEMA")
	private String codeThema;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "book_rubrica",
       joinColumns = {@JoinColumn(name = "book_id")},
       inverseJoinColumns = {@JoinColumn(name = "rubrica_id")})
    private List<Rubrica> rubricaSet;

	@Column(name = "COD_RUBRICA")
	private String codeRubrica;

	@Column(name = "AUTHOR")
	private String author;

	@Column(name = "PLACE")
	private String place;

	@Column(name = "PUBLISHER")
	private String publisher;

	@Column(name = "YEAR")
	private String year;

	@Column(name = "PAGES")
	private String pages;

	@Column(name = "COVERS")
	private String covers;

	@Column(name = "FORMAT")
	private String format;

	@Column(name = "COMMENT")
	private String comment;

	@Column(name = "PRICE")
	private int price;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "DESCRIPTION_COVERS")
	private String descriptionCovers;

	@Column(name = "STATE_CONDITION")
	private String condition;

	@Column(name = "DATA")
	private Date date;

	@Column(name = "STATE")
	private int state;

	@Column(name = "STATUS")
	private int status;

	@Column(name = "PAID")
	private boolean paid;

	@Column(name = "ID_ALIB")
	private int alibId;

	@Column(name = "SMALL_IMAGE")
	private String smallImage;

	@Column(name = "LARGE_IMAGE_1")
	private String largeImage1;

	@Column(name = "LARGE_IMAGE_2")
	private String largeImage2;

	@Column(name = "LARGE_IMAGE_3")
	private String largeImage3;

	@Column(name = "LARGE_IMAGE_4")
	private String largeImage4;

	@Column(name = "LARGE_IMAGE_5")
	private String largeImage5;

	@Column(name = "LARGE_IMAGE_6")
	private String largeImage6;

	@Column(name = "LARGE_IMAGE_7")
	private String largeImage7;

	@Column(name = "LARGE_IMAGE_8")
	private String largeImage8;

	@Column(name = "LARGE_IMAGE_9")
	private String largeImage9;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCodeThema() {
		return codeThema;
	}

	public void setCodeThema(String codeThema) {
		this.codeThema = codeThema;
	}

	public String getCodeRubrica() {
		return codeRubrica;
	}

	public void setCodeRubrica(String codeRubrica) {
		this.codeRubrica = codeRubrica;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public String getCovers() {
		return covers;
	}

	public void setCovers(String covers) {
		this.covers = covers;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionCovers() {
		return descriptionCovers;
	}

	public void setDescriptionCovers(String descriptionCovers) {
		this.descriptionCovers = descriptionCovers;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public int getAlibId() {
		return alibId;
	}

	public void setAlibId(int alibId) {
		this.alibId = alibId;
	}

	public String getSmallImage() {
		return smallImage;
	}

	public void setSmallImage(String smallImage) {
		this.smallImage = smallImage;
	}

	public String getLargeImage1() {
		return largeImage1;
	}

	public void setLargeImage1(String largeImage1) {
		this.largeImage1 = largeImage1;
	}

	public String getLargeImage2() {
		return largeImage2;
	}

	public void setLargeImage2(String largeImage2) {
		this.largeImage2 = largeImage2;
	}

	public String getLargeImage3() {
		return largeImage3;
	}

	public void setLargeImage3(String largeImage3) {
		this.largeImage3 = largeImage3;
	}

	public String getLargeImage4() {
		return largeImage4;
	}

	public void setLargeImage4(String largeImage4) {
		this.largeImage4 = largeImage4;
	}

	public String getLargeImage5() {
		return largeImage5;
	}

	public void setLargeImage5(String largeImage5) {
		this.largeImage5 = largeImage5;
	}

	public String getLargeImage6() {
		return largeImage6;
	}

	public void setLargeImage6(String largeImage6) {
		this.largeImage6 = largeImage6;
	}

	public String getLargeImage7() {
		return largeImage7;
	}

	public void setLargeImage7(String largeImage7) {
		this.largeImage7 = largeImage7;
	}

	public String getLargeImage8() {
		return largeImage8;
	}

	public void setLargeImage8(String largeImage8) {
		this.largeImage8 = largeImage8;
	}

	public String getLargeImage9() {
		return largeImage9;
	}

	public void setLargeImage9(String largeImage9) {
		this.largeImage9 = largeImage9;
	} 
	
	public List<Rubrica> getThemaSet() {
		return themaSet;
	}
	
	public List<Rubrica> getRubricaSet() {
		return rubricaSet;
	}
	
}
