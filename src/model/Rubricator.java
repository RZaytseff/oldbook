package model;

import entity.Rubrica;

public class Rubricator extends Rubrica {

	private static final long serialVersionUID = -1L;

	private int countThema;

	private int countRubrica;

	private boolean isThema;

	private boolean hasRubrices;

	public int getCountThema() {
		return countThema;
	}

	public void setCountThema(int countThema) {
		this.countThema = countThema;
	}

	public int getCountRubrica() {
		return countRubrica;
	}

	public void setCountRubrica(int countRubrica) {
		this.countRubrica = countRubrica;
	}

	public boolean isThema() {
		return isThema || getThemaCode().equalsIgnoreCase(getRubricaCode());
	}

	public boolean hasRubrices() {
		return hasRubrices;
	}

	public void setHasRubrices(boolean hasRubrices) {
		this.hasRubrices = hasRubrices;
	}

	public void setIsThema(boolean isThema) {
		this.isThema = isThema;
	}

}
