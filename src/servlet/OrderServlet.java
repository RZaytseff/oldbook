package servlet;

import java.io.IOException;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.naming.Context;

import bean.*;
import entity.Book;

@Stateless
public class OrderServlet  extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    @EJB(mappedName = "jndi:ext://OldBook/OrderBeanRemote") 
    private Order order; 				

    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String id = request.getParameter("id");
     	Book book = getOrderBean().getBook(Integer.parseInt(id)); book.getRubricaSet(); book.getThemaSet();
    	request.getSession().setAttribute("id", id);
    	request.getSession().setAttribute("isExists", book == null);
    	request.getSession().setAttribute("book", book); 
    	request.getSession().setAttribute("bookName", book.getName());
       	String fromPage = request.getParameter("fromPage");
        RequestDispatcher rd = request.getRequestDispatcher("Order.jsp?id=" + id + "&fromPage="+fromPage); 
        rd.forward(request, response); 
	}

	protected Order getOrderBean() {
		if(order == null)
			try {
				order = getGlobalOrderBean();
			} catch (ClassCastException | NamingException e) {
				try {
					order = getRemoteOrderBean();
				} catch (ClassCastException | NamingException e1) {
					e1.printStackTrace();
				}
			}
		return order;
	}
	
	protected Order getGlobalOrderBean() throws ClassCastException, NamingException {
		return (Order)new InitialContext().lookup("java:global/OldBook/OrderBean");
	}
	
	protected Order getRemoteOrderBean() throws ClassCastException, NamingException {
		Order order = null;
		Properties props = System.getProperties();
        props.put("javax.naming.InitialContext", "OldBook");
        
        props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.core.LocalInitialContextFactory");
        props.put("java.naming.factory.initial", "org.apache.openejb.client.RemoteInitialContextFactory");
        props.put("java.naming.provider.url", "http://127.0.0.1:8080/tomee/ejb");
        props.put("java.naming.security.principal",   "manager");
        props.put("java.naming.security.credentials", "manager");
		order = (Order)javax.rmi.PortableRemoteObject.narrow(
					new InitialContext(props).lookup("OrderBeanRemote"), OrderRemote.class);
//		order = (OrderRemote) orderHome.create();
			
		return order;
	}
	
}
